import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {LandingComponent} from './components/landing/landing.component';
import {DevNavComponent} from './components/dev-nav/dev-nav.component';
import {ProjectMainPageComponent} from './components/project-main-page/project-main-page.component';
import {EditMenuComponent} from './components/edit-menu/edit-menu.component';
import {AddEditItemComponent} from './components/add-edit-item/add-edit-item.component';
import {AddEditMenuCategoryComponent} from './components/add-edit-menu-category/add-edit-menu-category.component';
import {StopListComponent} from './components/stop-list/stop-list.component';
import {OrdersComponent} from './components/orders/orders.component';
import {SiteSectionsSettingsComponent} from './components/site-sections-settings/site-sections-settings.component';
import {SitePreviewComponent} from './components/site-preview/site-preview.component';
import {SiteGeneralSettingsComponent} from './components/site-general-settings/site-general-settings.component';
import {ClientMenuComponent} from './components/client-menu/client-menu.component';
import {ConstructorDevToMobComponent} from './components/constructor-dev-to-mob/constructor-dev-to-mob.component';

const routes: Routes = [
    {path: 'dev-nav', component: DevNavComponent},
    {path: 'landing', component: LandingComponent},
    {path: 'project-main', component: ProjectMainPageComponent},
    {path: 'menu', component: EditMenuComponent},
    {path: 'menu/edit/:id', component: AddEditItemComponent},
    {path: 'menu/new', component: AddEditItemComponent},
    {path: 'menu/category', component: AddEditMenuCategoryComponent},
    {path: 'stop-list', component: StopListComponent},
    {path: 'orders', component: OrdersComponent},
    {path: 'site-sections', component: SiteSectionsSettingsComponent},
    {path: 'preview', component: SitePreviewComponent},
    {path: 'general-settings', component: SiteGeneralSettingsComponent},
    {path: 'client-menu', component: ClientMenuComponent},
    {path: 'dev-mob', component: ConstructorDevToMobComponent},
    {path: '', redirectTo: '/dev-nav', pathMatch: 'full'}
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule {
}
