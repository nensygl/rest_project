export function maxLengthValidator(context: { requiredLength: string }): string {
    return `Максимальное количество символов — ${context.requiredLength}`;
}

export function minLengthValidator(context: { requiredLength: string }): string {
    return `Минимальное количество символов — ${context.requiredLength}`;
}
