import {DisplaySectionsInfoModel} from '../models/site-settings.model';
import {SectionsEnum} from '../enums/sections.enum';

export const MINUTE: number = 10;

export const MENU_ITEM_MAX_LEN = {
    NAME: 65,
    DESCRIPTION: 170,
    CATEGORY_NAME: 30
};

export const displaySectionsInfo: DisplaySectionsInfoModel[] = [
    {
        sectionId: SectionsEnum.cover,
        displayingItemNo: 1,
        sectionItemsLen: 2
    },
    {
        sectionId: SectionsEnum.buttons,
        displayingItemNo: 1,
        sectionItemsLen: 2
    },
    {
        sectionId: SectionsEnum.menu,
        displayingItemNo: 1,
        sectionItemsLen: 2
    },
    {
        sectionId: SectionsEnum.text,
        displayingItemNo: 1,
        sectionItemsLen: 4
    },
    {
        sectionId: SectionsEnum.banner,
        displayingItemNo: 1,
        sectionItemsLen: 2
    },
    {
        sectionId: SectionsEnum.gallery,
        displayingItemNo: 1,
        sectionItemsLen: 2
    },
    {
        sectionId: SectionsEnum.map,
        displayingItemNo: 1,
        sectionItemsLen: 2
    },
    {
        sectionId: SectionsEnum.contacts,
        displayingItemNo: 1,
        sectionItemsLen: 1
    },
];
