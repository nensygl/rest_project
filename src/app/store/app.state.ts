import {ILoginState} from './login';
import {IProjectState} from './project';
import {IOrdersState} from './orders';
import {ISiteSettingsState} from './site-settings';
import {IClientState} from './client';
import {ILandingState} from './landing';

export interface AppState {
    login: ILoginState,
    project: IProjectState,
    orders: IOrdersState,
    siteSettings: ISiteSettingsState,
    client: IClientState,
    landing: ILandingState
}
