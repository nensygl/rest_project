import {createActionGroup, emptyProps, props} from '@ngrx/store';
import {ProjectBaseInfoModel} from '../../models/project-base-info.model';
import {MenuCategoryModel, MenuItemModel, MenuModel} from '../../models/menu.model';

export const checkIsProjectCreated = createActionGroup({
  source: '[Project] Check Is Project Created',
  events: {
    'Request': emptyProps(),
    'Success': props<{isProjectCreated: boolean}>(),
    'Failure': (error: any) =>({error})
  }
});

export const createProject = createActionGroup({
  source: '[Project] Create Project',
  events: {
    'Request': emptyProps(),
    'Success': emptyProps(),
    'Failure': (error: any) =>({error})
  }
});

export const getProjectData = createActionGroup({
  source: '[Project] Get Project Data',
  events: {
    'Request': emptyProps(),
    'Success': props<{data: ProjectBaseInfoModel}>(),
    'Failure': (error: any) =>({error})
  }
});

export const getMenu = createActionGroup({
    source: '[Project] Get Menu',
    events: {
        'Request': emptyProps(),
        'Success': props<{menu: Array<MenuModel>}>(),
        'Failure': (error: any) =>({error})
    }
});

export const getMenuCategoriesList = createActionGroup({
    source: '[Project] Get Menu Categories List',
    events: {
        'Request': emptyProps(),
        'Success': props<{menuCategories: Array<MenuCategoryModel>}>(),
        'Failure': (error: any) =>({error})
    }
});

export const getMenuItemById = createActionGroup({
    source: '[Project] Get Menu Item By Id',
    events: {
        'Request': props<{id: number}>(),
        'Success': props<{menuItem: MenuItemModel}>(),
        'Failure': (error: any) =>({error})
    }
});

export const postMenuItem = createActionGroup({
    source: '[Project] Post Menu Item',
    events: {
        'Request': props<{menuItem: MenuItemModel}>(),
        'Success': props<{menuItem: MenuItemModel}>(),
        'Failure': (error: any) =>({error})
    }
});

export const deleteMenuItemById = createActionGroup({
    source: '[Project] Delete Menu Item By Id',
    events: {
        'Request': props<{id: number}>(),
        'Success': props<{success: boolean}>(),
        'Failure': (error: any) =>({error})
    }
});

export const deleteMenuItemsList = createActionGroup({
    source: '[Project] Delete Menu Items List',
    events: {
        'Request': props<{ids: number[]}>(),
        'Success': props<{success: boolean}>(),
        'Failure': (error: any) =>({error})
    }
});

export const hideMenuItemsList = createActionGroup({
    source: '[Project] Hide Menu Items List',
    events: {
        'Request': props<{ids: number[]}>(),
        'Success': props<{success: boolean}>(),
        'Failure': (error: any) =>({error})
    }
});

export const removeFromStopListItems = createActionGroup({
    source: '[Project] Remove From Stop List Items',
    events: {
        'Request': props<{ids: number[]}>(),
        'Success': props<{success: boolean}>(),
        'Failure': (error: any) =>({error})
    }
});

export const postCategoryItem = createActionGroup({
    source: '[Project] Post Category Item',
    events: {
        'Request': props<{ categoryItem: MenuCategoryModel }>(),
        'Success': props<{ menuCategories: MenuCategoryModel[] }>(),
        'Failure': (error: any) =>({ error })
    }
});

export const deleteCategoryItemById = createActionGroup({
    source: '[Project] Delete Category Item',
    events: {
        'Request': props<{ id: number }>(),
        'Success': props<{ menuCategories: MenuCategoryModel[] }>(),
        'Failure': (error: any) =>({ error })
    }
});

export const updateMenuCategoriesOrder = createActionGroup({
    source: '[Project] Update Menu Categories Order',
    events: {
        'Request': props<{ menuCategories: MenuCategoryModel[] }>(),
        'Success': props<{ menuCategories: MenuCategoryModel[] }>(),
        'Failure': (error: any) =>({ error })
    }
});
