import {createFeatureSelector, createSelector} from '@ngrx/store';
import {NgrxFeatures} from '../../enums/ngrx-features.enum';
import {IProjectState} from './project.state';

export class ProjectSelectors {
    static readonly getProjectState = createSelector(
        createFeatureSelector<IProjectState>(NgrxFeatures.project),
        state => state
    );

    static readonly getIsProjectCreated = createSelector(
        ProjectSelectors.getProjectState,
        state => state.isProjectCreated
    );

    static readonly getProjectBaseInfo = createSelector(
        ProjectSelectors.getProjectState,
        state => state.projectBaseInfo
    );

    static readonly getMenu = createSelector(
        ProjectSelectors.getProjectState,
        state => state.menu
    );

    static readonly getMenuCategories = createSelector(
        ProjectSelectors.getProjectState,
        state => state.menuCategories
    );

    static readonly getMenuItem = createSelector(
      ProjectSelectors.getProjectState,
      state => state.menuItem
    );
}
