import {Action, createReducer, on} from '@ngrx/store';
import {getInitialProjectState, IProjectState} from './project.state';
import * as ProjectActions from './project.actions';

const reducer = createReducer(
    getInitialProjectState(),
    on(ProjectActions.checkIsProjectCreated.success,
        (state, {isProjectCreated}) => ({...state, isProjectCreated})
    ),
    on(ProjectActions.getProjectData.success,
        (state, {data}) => ({...state, projectBaseInfo: data})
    ),
    on(ProjectActions.getMenu.success,
        (state, {menu}) => ({...state, menu})
    ),
    on(
        ProjectActions.getMenuCategoriesList.success,
        ProjectActions.postCategoryItem.success,
        ProjectActions.deleteCategoryItemById.success,
        ProjectActions.updateMenuCategoriesOrder.success,
        (state, {menuCategories}) => ({...state, menuCategories})
    ),
    on(ProjectActions.getMenuItemById.success,
        (state, {menuItem}) => ({...state, menuItem})
    ),
    on(ProjectActions.postMenuItem.success,
        (state, {menuItem}) => ({...state, menuItem})
    )
);

export function ProjectReducer(state: IProjectState | undefined, action: Action) {
    return reducer(state, action);
}
