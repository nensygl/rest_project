import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Actions, createEffect, ofType} from '@ngrx/effects';
import {ProjectService} from '../../services';
import {Router} from '@angular/router';
import * as ProjectActions from './project.actions';
import {catchError, map, of, switchMap} from 'rxjs';
import {ProjectBaseInfoModel} from '../../models/project-base-info.model';
import {MenuCategoryModel, MenuItemModel, MenuModel} from '../../models/menu.model';

@Injectable()
export class ProjectEffects {
    constructor(
        private http: HttpClient,
        private actions$: Actions,
        private service: ProjectService,
        private router: Router
    ) {
    }

    checkIsProjectCreated$ = createEffect(() =>
        this.actions$.pipe(
            ofType(ProjectActions.checkIsProjectCreated.request),
            switchMap(() =>
                this.service.checkIsProjectCreated().pipe(
                    map((isProjectCreated: boolean) =>
                        ProjectActions.checkIsProjectCreated.success({isProjectCreated})
                    ),
                    catchError((error) =>
                        of(ProjectActions.checkIsProjectCreated.failure({error}))
                    )
                )
            )
        )
    );

    getProjectData$ = createEffect(() =>
        this.actions$.pipe(
            ofType(
                ProjectActions.getProjectData.request,
                ProjectActions.checkIsProjectCreated.success
            ),
            switchMap(() =>
                this.service.getProjectBaseData().pipe(
                    map((data: ProjectBaseInfoModel) =>
                        ProjectActions.getProjectData.success({data})
                    ),
                    catchError((error) =>
                        of(ProjectActions.getProjectData.failure({error}))
                    )
                )
            )
        )
    );

    getMenu$ = createEffect(() =>
        this.actions$.pipe(
            ofType(
                ProjectActions.getMenu.request,
                ProjectActions.postMenuItem.success
            ),
            switchMap(() =>
                this.service.getMenu().pipe(
                    map((menu: Array<MenuModel>) =>
                        ProjectActions.getMenu.success({menu})
                    ),
                    catchError((error) =>
                        of(ProjectActions.getMenu.failure({error}))
                    )
                )
            )
        )
    );

    getMenuCategoriesList$ = createEffect(() =>
        this.actions$.pipe(
            ofType(ProjectActions.getMenuCategoriesList.request),
            switchMap(() =>
                this.service.getMenuCategoriesList().pipe(
                    map((menuCategories: Array<MenuCategoryModel>) =>
                        ProjectActions.getMenuCategoriesList.success({menuCategories})
                    ),
                    catchError((error) =>
                        of(ProjectActions.getMenuCategoriesList.failure({error}))
                    )
                )
            )
        )
    );

    getMenuItemById$ = createEffect(() =>
        this.actions$.pipe(
            ofType(ProjectActions.getMenuItemById.request),
            switchMap(({id}) =>
                this.service.getMenuItemById(id).pipe(
                    map((menuItem: MenuItemModel) =>
                        ProjectActions.getMenuItemById.success({menuItem})
                    ),
                    catchError((error) =>
                        of(ProjectActions.getMenuItemById.failure({error}))
                    )
                )
            )
        )
    );

    postMenuItem$ = createEffect(() =>
        this.actions$.pipe(
            ofType(ProjectActions.postMenuItem.request),
            switchMap(({menuItem}) =>
                this.service.postMenuItem(menuItem).pipe(
                    map((menuItem: MenuItemModel) =>
                        ProjectActions.postMenuItem.success({menuItem})
                    ),
                    catchError((error) =>
                        of(ProjectActions.postMenuItem.failure({error}))
                    )
                )
            )
        )
    );

    deleteMenuItemById$ = createEffect(() =>
        this.actions$.pipe(
            ofType(ProjectActions.deleteMenuItemById.request),
            switchMap(({id}) =>
                this.service.deleteMenuItemById(id).pipe(
                    map((success: boolean) => {
                            if (success) {
                                return ProjectActions.deleteMenuItemById.success({success});
                            }
                            return ProjectActions.deleteMenuItemById.failure({success});
                        }
                    ),
                    catchError((error) =>
                        of(ProjectActions.deleteMenuItemById.failure({error}))
                    )
                )
            )
        )
    );

    deleteMenuItemsList$ = createEffect(() =>
        this.actions$.pipe(
            ofType(ProjectActions.deleteMenuItemsList.request),
            switchMap(({ids}) =>
                this.service.deleteMenuItemsList(ids).pipe(
                    map((success: boolean) => {
                            if (success) {
                                return ProjectActions.deleteMenuItemsList.success({success});
                            }
                            return ProjectActions.deleteMenuItemsList.failure({success});
                        }
                    ),
                    catchError((error) =>
                        of(ProjectActions.deleteMenuItemsList.failure({error}))
                    )
                )
            )
        )
    );

    hideMenuItemsList$ = createEffect(() =>
        this.actions$.pipe(
            ofType(ProjectActions.hideMenuItemsList.request),
            switchMap(({ids}) =>
                this.service.hideMenuItemsList(ids).pipe(
                    map((success: boolean) => {
                            if (success) {
                                return ProjectActions.hideMenuItemsList.success({success});
                            }
                            return ProjectActions.hideMenuItemsList.failure({success});
                        }
                    ),
                    catchError((error) =>
                        of(ProjectActions.hideMenuItemsList.failure({error}))
                    )
                )
            )
        )
    );

    removeFromStopListItems$ = createEffect(() =>
        this.actions$.pipe(
            ofType(ProjectActions.removeFromStopListItems.request),
            switchMap(({ids}) =>
                this.service.removeFromStopListItems(ids).pipe(
                    map((success: boolean) => {
                        if (success) {
                            return ProjectActions.removeFromStopListItems.success({success});
                        }
                        return ProjectActions.removeFromStopListItems.failure({success});
                    }),
                    catchError((error) =>
                        of(ProjectActions.removeFromStopListItems.failure({error}))
                    )
                )
            )
        )
    );

    postCategoryItem$ = createEffect(() =>
        this.actions$.pipe(
            ofType(ProjectActions.postCategoryItem.request),
            switchMap(({categoryItem}) =>
                this.service.postCategoryItem(categoryItem).pipe(
                    map((menuCategories: MenuCategoryModel[]) =>
                        ProjectActions.postCategoryItem.success({menuCategories})
                    ),
                    catchError((error) =>
                        of(ProjectActions.postCategoryItem.failure({error}))
                    )
                )
            )
        )
    );

    deleteCategoryItemById$ = createEffect(() =>
        this.actions$.pipe(
            ofType(ProjectActions.deleteCategoryItemById.request),
            switchMap(({id}) =>
                this.service.deleteCategoryItem(id).pipe(
                    map((menuCategories: MenuCategoryModel[]) =>
                        ProjectActions.deleteCategoryItemById.success({menuCategories})
                    ),
                    catchError((error) =>
                        of(ProjectActions.deleteCategoryItemById.failure({error}))
                    )
                )
            )
        )
    );

    updateMenuCategoriesOrder$ = createEffect(() =>
        this.actions$.pipe(
            ofType(ProjectActions.updateMenuCategoriesOrder.request),
            switchMap(({menuCategories}) =>
                this.service.updateMenuCategoriesOrder(menuCategories).pipe(
                    map((menuCategories: MenuCategoryModel[]) =>
                        ProjectActions.updateMenuCategoriesOrder.success({menuCategories})
                    ),
                    catchError((error) =>
                        of(ProjectActions.updateMenuCategoriesOrder.failure({error}))
                    )
                )
            )
        )
    );
}
