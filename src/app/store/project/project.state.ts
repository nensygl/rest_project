import {ProjectBaseInfoModel} from '../../models/project-base-info.model';
import {MenuCategoryModel, MenuItemModel, MenuModel} from '../../models/menu.model';

export interface IProjectState {
    isProjectCreated: boolean;
    projectBaseInfo?: ProjectBaseInfoModel;
    menu?: Array<MenuModel>;
    menuItem: MenuItemModel;
    menuCategories?: Array<MenuCategoryModel>
}

export const INITIAL_STATE: IProjectState = {
    isProjectCreated: false,
    menuItem: new MenuItemModel()
};

export function getInitialProjectState(): IProjectState {
    return INITIAL_STATE;
}
