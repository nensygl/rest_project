import {Action, createReducer, on} from '@ngrx/store';
import * as LoginActions from './login.actions';
import {getInitialLoginState, ILoginState} from './login.state';

const reducer = createReducer(
  getInitialLoginState(),
  on(LoginActions.displayLoginModalChange,
    (state, {isDisplayLoginModal}) => {
    if (isDisplayLoginModal) {
      return {
        ...state,
        isDisplayLoginModal
      }
    }
    return {
      ...state,
      isDisplayLoginModal,
      isDisplayCodeInput: false
    }
    }),
  on(LoginActions.getSMSCodeSuccess,
    (state) => (
      {...state, isDisplayCodeInput: true}
    ))
);

export function LoginReducer(state: ILoginState | undefined, action: Action) {
  return reducer(state, action);
}
