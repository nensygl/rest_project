import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Actions, createEffect, ofType} from '@ngrx/effects';
import * as LoginActions from './login.actions';
import {catchError, map, of, switchMap, tap} from 'rxjs';
import {LoginService} from '../../services';
import {Router} from '@angular/router';

@Injectable()
export class LoginEffects {
  constructor(
    private http: HttpClient,
    private actions$: Actions,
    private service: LoginService,
    private router: Router
  ) {
  }

  getSMSCode$ = createEffect(() =>
    this.actions$.pipe(
      ofType(LoginActions.getSMSCode),
      switchMap(({phoneNum}) =>
        this.service.getSMSCode(phoneNum).pipe(
          map((isSuccess: boolean) =>
            // TODO change to normal action
            isSuccess ? LoginActions.getSMSCodeSuccess() : LoginActions.getSMSCodeSuccess()
          ),
          catchError((error) =>
            of(LoginActions.getSMSCodeFailure({error}))
          )
        )
      )
    )
  );

  checkSMSCode$ = createEffect(() =>
    this.actions$.pipe(
      ofType(LoginActions.checkSMSCode),
      switchMap(({code}) =>
        this.service.checkSMSCode(code).pipe(
          map((isValid: boolean) =>
            isValid ?
              LoginActions.checkSMSCodeSuccess() :
              LoginActions.showSMSCodeError()
          ),
          catchError((error) =>
            of(LoginActions.checkSMSCodeFailure({error}))
          )
        )
      )
    )
  );

  checkSMSCodeSuccess$ = createEffect(() =>
    this.actions$.pipe(
      ofType(LoginActions.checkSMSCodeSuccess),
      tap(() => this.router.navigate(['/']))
    ),
    {dispatch: false}
  );
}
