import {createFeatureSelector, createSelector} from '@ngrx/store';
import {ILoginState} from './login.state';
import {NgrxFeatures} from '../../enums/ngrx-features.enum';

export class LoginSelectors {
  static readonly getLoginState = createSelector(
    createFeatureSelector<ILoginState>(NgrxFeatures.login),
    state => state
  );

  static readonly getIsDisplayLoginModal = createSelector(
    LoginSelectors.getLoginState,
    state => state.isDisplayLoginModal
  );

  static readonly getIsDisplayCodeInput = createSelector(
    LoginSelectors.getLoginState,
    state => state.isDisplayCodeInput
  );

  static readonly getSmsCode = createSelector(
    LoginSelectors.getLoginState,
    state => state.smsCode
  );
}
