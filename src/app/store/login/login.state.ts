export interface ILoginState {
  isDisplayLoginModal: boolean;
  isDisplayCodeInput: boolean;
  smsCode: number | undefined;
}

export const INITIAL_STATE: ILoginState = {
  isDisplayLoginModal: false,
  isDisplayCodeInput: false,
  smsCode: undefined
}

export function getInitialLoginState(): ILoginState {
  return INITIAL_STATE;
}
