import {createAction, props} from '@ngrx/store';

const FEATURE_NAME: string = '[Login]';

export const displayLoginModalChange = createAction(
  `${FEATURE_NAME} Show Login Modal Change`,
  props<{isDisplayLoginModal: boolean}>()
);

export const getSMSCode = createAction(
  `${FEATURE_NAME} Get SMS Code`,
  props<{phoneNum: string}>()
);

export const getSMSCodeSuccess = createAction(
  `${FEATURE_NAME} Get SMS Code Success`
);

export const getSMSCodeFailure = createAction(
  `${FEATURE_NAME} Get SMS Code Failure`,
  props<{error: any}>()
);

export const checkSMSCode = createAction(
  `${FEATURE_NAME} Check SMS Code`,
  props<{code: string}>()
);

export const checkSMSCodeSuccess = createAction(
  `${FEATURE_NAME} Check SMS Code Success`
);

export const checkSMSCodeFailure = createAction(
  `${FEATURE_NAME} Check SMS Code Failure`,
  props<{error: any}>()
);

export const showSMSCodeError = createAction(
  `${FEATURE_NAME} Show SMS Code Failure`,
);
