import {Action, createReducer, on} from '@ngrx/store';
import {getInitialLandingState, ILandingState} from './landing.state';
import * as LandingActions from './landing.actions';

const reducer = createReducer(
    getInitialLandingState(),
    on(LandingActions.postCallbackForm.success,
        (state, {isFormSendSuccess}) => ({...state, isFormSendSuccess})
    )
);

export function LandingReducer(state: ILandingState | undefined, action: Action) {
    return reducer(state, action);
}
