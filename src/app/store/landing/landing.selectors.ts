import {createFeatureSelector, createSelector} from '@ngrx/store';
import {ILandingState} from './landing.state';
import {NgrxFeatures} from '../../enums/ngrx-features.enum';

export class LandingSelectors {
    static readonly getLandingState = createSelector(
        createFeatureSelector<ILandingState>(NgrxFeatures.landing),
        state => state
    );

    static readonly getIsFormSendSuccess = createSelector(
        LandingSelectors.getLandingState,
        state => state.isFormSendSuccess
    );
}
