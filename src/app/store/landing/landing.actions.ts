import {createActionGroup, props} from '@ngrx/store';
import {CallbackFormModel} from '../../models/landing.model';

export const postCallbackForm = createActionGroup({
    source: '[Landing] Post Callback Form',
    events: {
        'Request': props<{callbackForm: CallbackFormModel}>(),
        'Success': props<{isFormSendSuccess: boolean}>(),
        'Failure': (error: any) =>({error})
    }
});
