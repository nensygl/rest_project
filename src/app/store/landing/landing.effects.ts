import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Actions, createEffect, ofType} from '@ngrx/effects';
import {LandingService} from '../../services';
import {Router} from '@angular/router';
import * as LandingActions from './landing.actions';
import {catchError, map, of, switchMap} from 'rxjs';

@Injectable()
export class LandingEffects {
    constructor(
        private http: HttpClient,
        private actions$: Actions,
        private service: LandingService,
        private router: Router
    ) {
    }

    postCallbackForm$ = createEffect(() =>
        this.actions$.pipe(
            ofType(LandingActions.postCallbackForm.request),
            switchMap(({callbackForm}) =>
                this.service.postCallbackForm(callbackForm).pipe(
                    map((isFormSendSuccess: boolean) =>
                        LandingActions.postCallbackForm.success({isFormSendSuccess})
                    ),
                    catchError((error) =>
                        of(LandingActions.postCallbackForm.failure({error}))
                    )
                )
            )
        )
    );
}
