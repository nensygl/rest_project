export interface ILandingState {
    isFormSendSuccess: boolean;
}

export const INITIAL_STATE: ILandingState = {
    isFormSendSuccess: false
}

export function getInitialLandingState(): ILandingState {
    return INITIAL_STATE;
}
