import {ActionReducerMap, MetaReducer} from '@ngrx/store';
import {AppState} from './app.state';
import * as LoginReducer from './login/login.reducer';
import * as ProjectReducer from './project/project.reducer';
import * as OrdersReducer from './orders/orders.reducer';
import * as SiteSettingsReducer from './site-settings/site-settings.reducer';
import * as ClientReducer from './client/client.reducer';
import * as LandingReducer from './landing/landing.reducer';

export const AppReducer: ActionReducerMap<AppState> = {
    login: LoginReducer.LoginReducer,
    project: ProjectReducer.ProjectReducer,
    orders: OrdersReducer.OrdersReducer,
    siteSettings: SiteSettingsReducer.SiteSettingsReducer,
    client: ClientReducer.ClientReducer,
    landing: LandingReducer.LandingReducer
};

export const metaReducers: MetaReducer<AppState>[] = [];
