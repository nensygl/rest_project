import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Actions, createEffect, ofType} from '@ngrx/effects';
import {SiteSettingsService} from '../../services';
import {Router} from '@angular/router';
import * as SiteSettingsActions from './site-settings.actions';
import {catchError, map, of, switchMap} from 'rxjs';
import {SelectedSectionModel} from '../../models/site-settings.model';
import {
    FontListItemModel, FontWeightItemModel,
    PaymentSystemSettingsModel,
    SiteColorsAndFontsSettingsModel,
    SiteDomainSettingsModel,
    SiteGeneralSettingsModel, SiteSettingsDataModel, UserSubscriptionDataModel
} from '../../models/site-general-settings.model';

@Injectable()
export class SiteSettingsEffects {
    constructor(
        private http: HttpClient,
        private actions$: Actions,
        private service: SiteSettingsService,
        private router: Router
    ) {
    }

    getSelectedSections$ = createEffect(() =>
        this.actions$.pipe(
            ofType(SiteSettingsActions.getSelectedSections.request),
            switchMap(() =>
                this.service.getSelectedSections().pipe(
                    map((selectedSections: SelectedSectionModel[]) =>
                        SiteSettingsActions.getSelectedSections.success({selectedSections})
                    ),
                    catchError((error) =>
                        of(SiteSettingsActions.getSelectedSections.failure({error}))
                    )
                )
            )
        )
    );

    updateSelectedSections$ = createEffect(() =>
        this.actions$.pipe(
            ofType(SiteSettingsActions.updateSelectedSections.request),
            switchMap(({selectedSections}) =>
                this.service.updateSelectedSections(selectedSections).pipe(
                    map((selectedSections: SelectedSectionModel[]) =>
                        SiteSettingsActions.updateSelectedSections.success({selectedSections})
                    ),
                    catchError((error) =>
                        of(SiteSettingsActions.updateSelectedSections.failure({error}))
                    )
                )
            )
        )
    );

    getGeneralSettingsData$ = createEffect(() =>
        this.actions$.pipe(
            ofType(SiteSettingsActions.getGeneralSettingsData.request),
            switchMap(() =>
                this.service.getGeneralSettingsData().pipe(
                    map((generalData: SiteGeneralSettingsModel) =>
                        SiteSettingsActions.getGeneralSettingsData.success({generalData})
                    ),
                    catchError((error) =>
                        of(SiteSettingsActions.getGeneralSettingsData.failure({error}))
                    )
                )
            )
        )
    );

    updateGeneralSettingsData$ = createEffect(() =>
        this.actions$.pipe(
            ofType(SiteSettingsActions.updateGeneralSettingsData.request),
            switchMap(({generalData}) =>
                this.service.updateGeneralSettingsData(generalData).pipe(
                    map((generalData: SiteGeneralSettingsModel) =>
                        SiteSettingsActions.updateGeneralSettingsData.success({generalData})
                    ),
                    catchError((error) =>
                        of(SiteSettingsActions.updateGeneralSettingsData.failure({error}))
                    )
                )
            )
        )
    );

    getColorsAndFontsSettingsData$ = createEffect(() =>
        this.actions$.pipe(
            ofType(SiteSettingsActions.getColorsAndFontsSettingsData.request),
            switchMap(() =>
                this.service.getColorsAndFontsData().pipe(
                    map((colorsAndFonts: SiteColorsAndFontsSettingsModel) =>
                        SiteSettingsActions.getColorsAndFontsSettingsData.success({colorsAndFonts})
                    ),
                    catchError((error) =>
                        of(SiteSettingsActions.getColorsAndFontsSettingsData.failure({error}))
                    )
                )
            )
        )
    );

    updateColorsAndFontsSettingsData$ = createEffect(() =>
        this.actions$.pipe(
            ofType(SiteSettingsActions.updateColorsAndFontsSettingsData.request),
            switchMap(({colorsAndFonts}) =>
                this.service.updateColorsAndFontsData(colorsAndFonts).pipe(
                    map((colorsAndFonts: SiteColorsAndFontsSettingsModel) =>
                        SiteSettingsActions.updateColorsAndFontsSettingsData.success({colorsAndFonts})
                    ),
                    catchError((error) =>
                        of(SiteSettingsActions.updateColorsAndFontsSettingsData.failure({error}))
                    )
                )
            )
        )
    );

    getFontsList$ = createEffect(() =>
        this.actions$.pipe(
            ofType(SiteSettingsActions.getFontsList.request),
            switchMap(() =>
                this.service.getFontsList().pipe(
                    map((fontsList: FontListItemModel[]) =>
                        SiteSettingsActions.getFontsList.success({fontsList})
                    ),
                    catchError((error) =>
                        of(SiteSettingsActions.getFontsList.failure({error}))
                    )
                )
            )
        )
    );

    getFontWeightList$ = createEffect(() =>
        this.actions$.pipe(
            ofType(SiteSettingsActions.getFontWeightList.request),
            switchMap(() =>
                this.service.getFontWeightList().pipe(
                    map((fontWeightList: FontWeightItemModel[]) =>
                        SiteSettingsActions.getFontWeightList.success({fontWeightList})
                    ),
                    catchError((error) =>
                        of(SiteSettingsActions.getFontWeightList.failure({error}))
                    )
                )
            )
        )
    );

    getSelectedMenuCardsStyle$ = createEffect(() =>
        this.actions$.pipe(
            ofType(SiteSettingsActions.getSelectedMenuCardsStyle.request),
            switchMap(() =>
                this.service.getSelectedMenuCardsStyle().pipe(
                    map((selectedMenuCardsStyle: number) =>
                        SiteSettingsActions.getSelectedMenuCardsStyle.success({selectedMenuCardsStyle})
                    ),
                    catchError((error) =>
                        of(SiteSettingsActions.getSelectedMenuCardsStyle.failure({error}))
                    )
                )
            )
        )
    );

    updateSelectedMenuCardsStyle$ = createEffect(() =>
        this.actions$.pipe(
            ofType(SiteSettingsActions.updateSelectedMenuCardsStyle.request),
            switchMap(({selectedMenuCardsStyle}) =>
                this.service.updateSelectedMenuCardsStyle(selectedMenuCardsStyle).pipe(
                    map((selectedMenuCardsStyle: number) =>
                        SiteSettingsActions.updateSelectedMenuCardsStyle.success({selectedMenuCardsStyle})
                    ),
                    catchError((error) =>
                        of(SiteSettingsActions.updateSelectedMenuCardsStyle.failure({error}))
                    )
                )
            )
        )
    );

    getMainPageData$ = createEffect(() =>
        this.actions$.pipe(
            ofType(SiteSettingsActions.getMainPageData.request),
            switchMap(() =>
                this.service.getMainPageDataData().pipe(
                    map((mainPageData: SiteSettingsDataModel[]) =>
                        SiteSettingsActions.getMainPageData.success({ mainPageData })
                    ),
                    catchError((error) =>
                        of(SiteSettingsActions.getMainPageData.failure({ error }))
                    )
                )
            )
        )
    );

    updateMainPageData$ = createEffect(() =>
        this.actions$.pipe(
            ofType(SiteSettingsActions.updateMainPageData.request),
            switchMap(({ mainPageData }) =>
                this.service.updateMainPageData(mainPageData).pipe(
                    map((mainPageData: SiteSettingsDataModel[]) =>
                        SiteSettingsActions.updateMainPageData.success({ mainPageData })
                    ),
                    catchError((error) =>
                        of(SiteSettingsActions.updateMainPageData.failure({ error }))
                    )
                )
            )
        )
    );

    getSiteDomainData$ = createEffect(() =>
        this.actions$.pipe(
            ofType(SiteSettingsActions.getSiteDomainData.request),
            switchMap(() =>
                this.service.getSiteDomainData().pipe(
                    map((siteDomainData: SiteDomainSettingsModel) =>
                        SiteSettingsActions.getSiteDomainData.success({siteDomainData})
                    ),
                    catchError((error) =>
                        of(SiteSettingsActions.getSiteDomainData.failure({error}))
                    )
                )
            )
        )
    );

    updateSiteDomainData$ = createEffect(() =>
        this.actions$.pipe(
            ofType(SiteSettingsActions.updateSiteDomainData.request),
            switchMap(({siteDomainData}) =>
                this.service.updateSiteDomainData(siteDomainData).pipe(
                    map((siteDomainData: SiteDomainSettingsModel) =>
                        SiteSettingsActions.updateSiteDomainData.success({siteDomainData})
                    ),
                    catchError((error) =>
                        of(SiteSettingsActions.updateSiteDomainData.failure({error}))
                    )
                )
            )
        )
    );

    checkIsDomainAvailable$ = createEffect(() =>
        this.actions$.pipe(
            ofType(SiteSettingsActions.checkIsDomainAvailable.request),
            switchMap(({domainName}) =>
                this.service.checkIsDomainAvailable(domainName).pipe(
                    map((isDomainAvailable: boolean) =>
                        SiteSettingsActions.checkIsDomainAvailable.success({isDomainAvailable})
                    ),
                    catchError((error) =>
                        of(SiteSettingsActions.checkIsDomainAvailable.failure({error}))
                    )
                )
            )
        )
    );

    getPaymentSystemData$ = createEffect(() =>
        this.actions$.pipe(
            ofType(SiteSettingsActions.getPaymentSystemData.request),
            switchMap(() =>
                this.service.getPaymentSystemData().pipe(
                    map((paymentSystemData: PaymentSystemSettingsModel) =>
                        SiteSettingsActions.getPaymentSystemData.success({paymentSystemData})
                    ),
                    catchError((error) =>
                        of(SiteSettingsActions.getPaymentSystemData.failure({error}))
                    )
                )
            )
        )
    );

    updatePaymentSystemData$ = createEffect(() =>
        this.actions$.pipe(
            ofType(SiteSettingsActions.updatePaymentSystemData.request),
            switchMap(({paymentSystemData}) =>
                this.service.updatePaymentSystemData(paymentSystemData).pipe(
                    map((paymentSystemData: PaymentSystemSettingsModel) =>
                        SiteSettingsActions.updatePaymentSystemData.success({paymentSystemData})
                    ),
                    catchError((error) =>
                        of(SiteSettingsActions.updatePaymentSystemData.failure({error}))
                    )
                )
            )
        )
    );

    getSubscriptionData$ = createEffect(() =>
        this.actions$.pipe(
            ofType(SiteSettingsActions.getSubscriptionData.request),
            switchMap(() =>
                this.service.getSubscriptionData().pipe(
                    map((subscriptionData: UserSubscriptionDataModel) =>
                        SiteSettingsActions.getSubscriptionData.success({subscriptionData})
                    ),
                    catchError((error) =>
                        of(SiteSettingsActions.getSubscriptionData.failure({error}))
                    )
                )
            )
        )
    );

    cancelSubscription$ = createEffect(() =>
        this.actions$.pipe(
            ofType(SiteSettingsActions.cancelSubscription.request),
            switchMap(() =>
                this.service.cancelSubscription().pipe(
                    map((subscriptionData: UserSubscriptionDataModel) =>
                        SiteSettingsActions.cancelSubscription.success({subscriptionData})
                    ),
                    catchError((error) =>
                        of(SiteSettingsActions.cancelSubscription.failure({error}))
                    )
                )
            )
        )
    );
}
