import {createFeatureSelector, createSelector} from '@ngrx/store';
import {ISiteSettingsState} from './site-settings.state';
import {NgrxFeatures} from '../../enums/ngrx-features.enum';

export class SiteSettingsSelectors {
    static readonly getSiteSettingsState = createSelector(
        createFeatureSelector<ISiteSettingsState>(NgrxFeatures.siteSettings),
        state => state
    );

    static readonly getSelectedSections = createSelector(
        SiteSettingsSelectors.getSiteSettingsState,
        state => state.selectedSections
    );

    static readonly getGeneralData = createSelector(
        SiteSettingsSelectors.getSiteSettingsState,
        state => state.generalData
    );

    static readonly getColorsAndFontsData = createSelector(
        SiteSettingsSelectors.getSiteSettingsState,
        state => state.colorsAndFonts
    );

    static readonly getFontsList = createSelector(
        SiteSettingsSelectors.getSiteSettingsState,
        state => state.fontsList
    );

    static readonly getFontWeightList = createSelector(
        SiteSettingsSelectors.getSiteSettingsState,
        state => state.fontWeightList
    );

    static readonly getSelectedMenuCardsStyle = createSelector(
        SiteSettingsSelectors.getSiteSettingsState,
        state => state.selectedMenuCardsStyle
    );

    static readonly getMainPageData = createSelector(
        SiteSettingsSelectors.getSiteSettingsState,
        state => state.mainPageData
    );

    static readonly getSiteDomainData = createSelector(
        SiteSettingsSelectors.getSiteSettingsState,
        state => state.siteDomainData
    );

    static readonly getIsDomainAvailable = createSelector(
        SiteSettingsSelectors.getSiteSettingsState,
        state => state.isDomainAvailable
    );

    static readonly getPaymentSystemData = createSelector(
        SiteSettingsSelectors.getSiteSettingsState,
        state => state.paymentSystemData
    );

    static readonly getSubscriptionData = createSelector(
        SiteSettingsSelectors.getSiteSettingsState,
        state => state.subscriptionData
    );
}
