import {Action, createReducer, on} from '@ngrx/store';
import {getInitialSiteSettingsState, ISiteSettingsState} from './site-settings.state';
import * as SiteSettingsActions from './site-settings.actions';

const reducer = createReducer(
    getInitialSiteSettingsState(),
    on(
        SiteSettingsActions.getSelectedSections.success,
        SiteSettingsActions.updateSelectedSections.success,
        (state, {selectedSections}) => ({...state, selectedSections})
    ),
    on(
        SiteSettingsActions.getGeneralSettingsData.success,
        SiteSettingsActions.updateGeneralSettingsData.success,
        (state, {generalData}) => ({...state, generalData})
    ),
    on(
        SiteSettingsActions.getColorsAndFontsSettingsData.success,
        SiteSettingsActions.updateColorsAndFontsSettingsData.success,
        (state, {colorsAndFonts}) => ({...state, colorsAndFonts})
    ),
    on(
        SiteSettingsActions.getFontsList.success,
        (state, {fontsList}) => ({...state, fontsList})
    ),
    on(
        SiteSettingsActions.getFontWeightList.success,
        (state, {fontWeightList}) => ({...state, fontWeightList})
    ),
    on(
        SiteSettingsActions.getSelectedMenuCardsStyle.success,
        SiteSettingsActions.updateSelectedMenuCardsStyle.success,
        (state, {selectedMenuCardsStyle}) => ({...state, selectedMenuCardsStyle})
    ),
    on(
        SiteSettingsActions.getMainPageData.success,
        // SiteSettingsActions.updateMainPageData.success,
        (state, {mainPageData}) => ({...state, mainPageData})
    ),
    on(
        SiteSettingsActions.getPaymentSystemData.success,
        SiteSettingsActions.updatePaymentSystemData.success,
        (state, {paymentSystemData}) => ({...state, paymentSystemData})
    ),
    on(
        SiteSettingsActions.getSiteDomainData.success,
        SiteSettingsActions.updateSiteDomainData.success,
        (state, {siteDomainData}) => ({...state, siteDomainData})
    ),
    on(
        SiteSettingsActions.checkIsDomainAvailable.success,
        (state, {isDomainAvailable}) => ({...state, isDomainAvailable})
    ),
    on(
        SiteSettingsActions.getSubscriptionData.success,
        SiteSettingsActions.cancelSubscription.success,
        (state, {subscriptionData}) => ({...state, subscriptionData})
    )
);

export function SiteSettingsReducer(state: ISiteSettingsState | undefined, action: Action) {
    return reducer(state, action);
}
