import {SelectedSectionModel} from '../../models/site-settings.model';
import {
    FontListItemModel, FontWeightItemModel,
    PaymentSystemSettingsModel,
    SiteColorsAndFontsSettingsModel, SiteDomainSettingsModel,
    SiteGeneralSettingsModel, UserSubscriptionDataModel
} from '../../models/site-general-settings.model';

export interface ISiteSettingsState {
    selectedSections: SelectedSectionModel[],
    generalData?: SiteGeneralSettingsModel,
    colorsAndFonts?: SiteColorsAndFontsSettingsModel,
    fontsList?: FontListItemModel[],
    fontWeightList?: FontWeightItemModel[],
    selectedMenuCardsStyle?: number,
    mainPageData?: any,
    siteDomainData?: SiteDomainSettingsModel,
    isDomainAvailable?: boolean,
    paymentSystemData?: PaymentSystemSettingsModel,
    subscriptionData?: UserSubscriptionDataModel
}

export const INITIAL_STATE: ISiteSettingsState = {
    selectedSections: []
};

export function getInitialSiteSettingsState(): ISiteSettingsState {
    return INITIAL_STATE;
}
