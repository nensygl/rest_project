import {createActionGroup, emptyProps, props} from '@ngrx/store';
import {SelectedSectionModel} from '../../models/site-settings.model';
import {
    FontListItemModel, FontWeightItemModel,
    PaymentSystemSettingsModel,
    SiteColorsAndFontsSettingsModel, SiteDomainSettingsModel,
    SiteGeneralSettingsModel, SiteSettingsDataModel, UserSubscriptionDataModel
} from '../../models/site-general-settings.model';

export const getSelectedSections = createActionGroup({
    source: '[Site Settings] Get Selected Sections',
    events: {
        'Request': emptyProps(),
        'Success': props<{selectedSections: SelectedSectionModel[]}>(),
        'Failure': (error: any) =>({error})
    }
});

export const updateSelectedSections = createActionGroup({
    source: '[Site Settings] Update Selected Sections',
    events: {
        'Request': props<{selectedSections: SelectedSectionModel[]}>(),
        'Success': props<{selectedSections: SelectedSectionModel[]}>(),
        'Failure': (error: any) =>({error})
    }
});

export const getGeneralSettingsData = createActionGroup({
    source: '[Site Settings] Get General Settings Data',
    events: {
        'Request': emptyProps(),
        'Success': props<{generalData: SiteGeneralSettingsModel}>(),
        'Failure': (error: any) =>({error})
    }
});

export const updateGeneralSettingsData = createActionGroup({
    source: '[Site Settings] Update General Settings Data',
    events: {
        'Request': props<{generalData: SiteGeneralSettingsModel}>(),
        'Success': props<{generalData: SiteGeneralSettingsModel}>(),
        'Failure': (error: any) =>({error})
    }
});

export const getColorsAndFontsSettingsData = createActionGroup({
    source: '[Site Settings] Get Colors And Fonts Settings Data',
    events: {
        'Request': emptyProps(),
        'Success': props<{colorsAndFonts: SiteColorsAndFontsSettingsModel}>(),
        'Failure': (error: any) =>({error})
    }
});

export const updateColorsAndFontsSettingsData = createActionGroup({
    source: '[Site Settings] Update Colors And Fonts Settings Data',
    events: {
        'Request': props<{colorsAndFonts: SiteColorsAndFontsSettingsModel}>(),
        'Success': props<{colorsAndFonts: SiteColorsAndFontsSettingsModel}>(),
        'Failure': (error: any) =>({error})
    }
});

export const getFontsList = createActionGroup({
    source: '[Site Settings] Get Fonts List',
    events: {
        'Request': emptyProps(),
        'Success': props<{fontsList: FontListItemModel[]}>(),
        'Failure': (error: any) =>({error})
    }
});

export const getFontWeightList = createActionGroup({
    source: '[Site Settings] Get Font Weight List',
    events: {
        'Request': emptyProps(),
        'Success': props<{fontWeightList: FontWeightItemModel[]}>(),
        'Failure': (error: any) =>({error})
    }
});

export const getSelectedMenuCardsStyle = createActionGroup({
    source: '[Site Settings] Get Selected Menu Cards Style',
    events: {
        'Request': emptyProps(),
        'Success': props<{selectedMenuCardsStyle: number}>(),
        'Failure': (error: any) =>({error})
    }
});

export const updateSelectedMenuCardsStyle = createActionGroup({
    source: '[Site Settings] Update Selected Menu Cards Style',
    events: {
        'Request': props<{selectedMenuCardsStyle: number}>(),
        'Success': props<{selectedMenuCardsStyle: number}>(),
        'Failure': (error: any) =>({error})
    }
});

export const getMainPageData = createActionGroup({
    source: '[Site Settings] Get Main Page Data',
    events: {
        'Request': emptyProps(),
        'Success': props<{mainPageData: SiteSettingsDataModel[]}>(),
        'Failure': (error: any) =>({error})
    }
});

export const updateMainPageData = createActionGroup({
    source: '[Site Settings] Update Main Page Data',
    events: {
        'Request': props<{mainPageData: any}>(),
        'Success': props<{mainPageData: any}>(),
        'Failure': (error: any) =>({error})
    }
});

export const getSiteDomainData = createActionGroup({
    source: '[Site Settings] Get Site Domain Data',
    events: {
        'Request': emptyProps(),
        'Success': props<{siteDomainData: SiteDomainSettingsModel}>(),
        'Failure': (error: any) =>({error})
    }
});

export const updateSiteDomainData = createActionGroup({
    source: '[Site Settings] Update Site Domain Data',
    events: {
        'Request': props<{siteDomainData: SiteDomainSettingsModel}>(),
        'Success': props<{siteDomainData: SiteDomainSettingsModel}>(),
        'Failure': (error: any) =>({error})
    }
});

export const checkIsDomainAvailable = createActionGroup({
    source: '[Site Settings] Check Is Domain Available',
    events: {
        'Request': props<{domainName: string}>(),
        'Success': props<{isDomainAvailable: boolean}>(),
        'Failure': (error: any) =>({error})
    }
});

export const getPaymentSystemData = createActionGroup({
    source: '[Site Settings] Get Payment System Data',
    events: {
        'Request': emptyProps(),
        'Success': props<{paymentSystemData: PaymentSystemSettingsModel}>(),
        'Failure': (error: any) =>({error})
    }
});

export const updatePaymentSystemData = createActionGroup({
    source: '[Site Settings] Update Payment System Data',
    events: {
        'Request': props<{paymentSystemData: PaymentSystemSettingsModel}>(),
        'Success': props<{paymentSystemData: PaymentSystemSettingsModel}>(),
        'Failure': (error: any) =>({error})
    }
});

export const getSubscriptionData = createActionGroup({
    source: '[Site Settings] Get Subscription Data',
    events: {
        'Request': emptyProps(),
        'Success': props<{subscriptionData: UserSubscriptionDataModel}>(),
        'Failure': (error: any) =>({error})
    }
});

export const cancelSubscription = createActionGroup({
    source: '[Site Settings] Cancel Subscription',
    events: {
        'Request': emptyProps(),
        'Success': props<{subscriptionData: UserSubscriptionDataModel}>(),
        'Failure': (error: any) =>({error})
    }
});
