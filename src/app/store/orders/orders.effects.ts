import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Actions, createEffect, ofType} from '@ngrx/effects';
import {OrdersService} from '../../services';
import * as OrdersActions from './orders.actions';
import {Router} from '@angular/router';
import {catchError, map, mergeMap, of, switchMap} from 'rxjs';
import {OrderListModel, OrderModel} from '../../models/orders.model';

@Injectable()
export class OrdersEffects {
    constructor(
        private http: HttpClient,
        private actions$: Actions,
        private service: OrdersService,
        private router: Router
    ) {
    }

    getOrdersList$ = createEffect(() =>
        this.actions$.pipe(
            ofType(OrdersActions.getOrdersList.request),
            switchMap(({filters}) =>
                this.service.getOrdersList(filters).pipe(
                    map((ordersList: OrderListModel[]) =>
                        OrdersActions.getOrdersList.success({ordersList})
                    ),
                    catchError((error) =>
                        of(OrdersActions.getOrdersList.failure({error}))
                    )
                )
            )
        )
    );

    getOrderById$ = createEffect(() =>
        this.actions$.pipe(
            ofType(OrdersActions.getOrderById.request),
            mergeMap(({id}) =>
                this.service.getOrderInfoById(id).pipe(
                    map((selectedOrder: OrderModel) =>
                        OrdersActions.getOrderById.success({selectedOrder})
                    ),
                    catchError((error) =>
                        of(OrdersActions.getOrderById.failure({error}))
                    )
                )
            )
        )
    );

    cancelOrder$ = createEffect(() =>
        this.actions$.pipe(
            ofType(OrdersActions.cancelOrder.request),
            switchMap(({id}) =>
                this.service.cancelOrderById(id).pipe(
                    map((selectedOrder: OrderModel) =>
                        OrdersActions.cancelOrder.success({selectedOrder})
                    ),
                    catchError((error) =>
                        of(OrdersActions.cancelOrder.failure({error}))
                    )
                )
            )
        )
    );

    updateOrder$ = createEffect(() =>
        this.actions$.pipe(
            ofType(OrdersActions.updateOrder.request),
            mergeMap(({selectedOrder}) =>
                this.service.updateOrder(selectedOrder).pipe(
                    map((selectedOrder: OrderModel) =>
                        OrdersActions.updateOrder.success({selectedOrder})
                    ),
                    catchError((error) =>
                        of(OrdersActions.updateOrder.failure({error}))
                    )
                )
            )
        )
    );
}
