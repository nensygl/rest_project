import {Action, createReducer, on} from '@ngrx/store';
import {getInitialOrdersState, IOrdersState} from './orders.state';
import * as OrdersActions from './orders.actions';

const reducer = createReducer(
    getInitialOrdersState(),
    on(OrdersActions.getOrdersList.success,
        (state, {ordersList}) => ({...state, ordersList})
    ),
    on(OrdersActions.getOrderById.success,
        OrdersActions.cancelOrder.success,
        OrdersActions.updateOrder.success,
        (state, {selectedOrder}) => ({...state, selectedOrder})
    ),
);

export function OrdersReducer(state: IOrdersState | undefined, action: Action) {
    return reducer(state, action);
}
