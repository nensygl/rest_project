import {createFeatureSelector, createSelector} from '@ngrx/store';
import {IOrdersState} from './orders.state';
import {NgrxFeatures} from '../../enums/ngrx-features.enum';

export class OrdersSelectors {
    static readonly getOrdersState = createSelector(
        createFeatureSelector<IOrdersState>(NgrxFeatures.orders),
        state => state
    );

    static readonly getOrdersList = createSelector(
        OrdersSelectors.getOrdersState,
        state => state.ordersList
    );

    static readonly getSelectedOrder = createSelector(
        OrdersSelectors.getOrdersState,
        state => state.selectedOrder
    );
}
