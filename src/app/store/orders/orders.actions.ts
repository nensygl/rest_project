import {createActionGroup, props} from '@ngrx/store';
import {OrderListModel, OrderModel, OrdersListFiltersModel} from '../../models/orders.model';

export const getOrdersList = createActionGroup({
    source: '[Orders] Get Orders List',
    events: {
        'Request': props<{filters: OrdersListFiltersModel}>(),
        'Success': props<{ordersList: OrderListModel[]}>(),
        'Failure': (error: any) =>({error})
    }
});

export const getOrderById = createActionGroup({
    source: '[Orders] Get Order By Id',
    events: {
        'Request': props<{id: number}>(),
        'Success': props<{selectedOrder: OrderModel}>(),
        'Failure': (error: any) =>({error})
    }
});

export const cancelOrder = createActionGroup({
    source: '[Orders] Cancel Order',
    events: {
        'Request': props<{id: number}>(),
        'Success': props<{selectedOrder: OrderModel}>(),
        'Failure': (error: any) =>({error})
    }
});

export const updateOrder = createActionGroup({
    source: '[Orders] Update Order',
    events: {
        'Request': props<{selectedOrder: OrderModel}>(),
        'Success': props<{selectedOrder: OrderModel}>(),
        'Failure': (error: any) =>({error})
    }
});
