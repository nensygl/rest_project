import {OrderListModel, OrderModel} from '../../models/orders.model';

export interface IOrdersState {
    ordersList: OrderListModel[];
    selectedOrder?: OrderModel;
}

export const INITIAL_STATE: IOrdersState = {
    ordersList: []
}

export function getInitialOrdersState(): IOrdersState {
    return INITIAL_STATE;
}
