import {createFeatureSelector, createSelector} from '@ngrx/store';
import {IClientState} from './client.state';
import {NgrxFeatures} from '../../enums/ngrx-features.enum';

export class ClientSelectors {
    static readonly getClientState = createSelector(
        createFeatureSelector<IClientState>(NgrxFeatures.client),
        state => state
    );

    static readonly getMenu = createSelector(
        ClientSelectors.getClientState,
        state => state.menu
    );

    static readonly getCart = createSelector(
        ClientSelectors.getClientState,
        state => state.cart
    );

    static readonly getDisplayCart = createSelector(
        ClientSelectors.getClientState,
        state => state.displayCart
    );
}
