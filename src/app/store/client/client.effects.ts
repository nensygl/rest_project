import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Actions, createEffect, ofType} from '@ngrx/effects';
import {ClientService} from '../../services';
import {Router} from '@angular/router';
import * as ClientActions from '../client/client.actions';
import {catchError, map, of, switchMap} from 'rxjs';
import {MenuModel} from '../../models/menu.model';

@Injectable()
export class ClientEffects {
    constructor(
        private http: HttpClient,
        private actions$: Actions,
        private service: ClientService,
        private router: Router
    ) {
    }

    getMenu$ = createEffect(() =>
        this.actions$.pipe(
            ofType(ClientActions.getMenu.request),
            switchMap(() =>
                this.service.getMenu().pipe(
                    map((menu: MenuModel[]) =>
                        ClientActions.getMenu.success({menu})
                    ),
                    catchError((error) =>
                        of(ClientActions.getMenu.failure({error}))
                    )
                )
            )
        )
    );
}
