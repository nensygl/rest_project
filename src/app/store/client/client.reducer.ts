import {Action, createReducer, on} from '@ngrx/store';
import {getInitialClientState, IClientState} from './client.state';
import * as ClientActions from './client.actions';
import {MenuItemModel} from '../../models/menu.model';

const reducer = createReducer(
    getInitialClientState(),
    on(
        ClientActions.getMenu.success,
        (state, {menu}) => ({...state, menu})
    ),
    on(
        ClientActions.addItemToCart,
        (state, {item}) => {
            let quantity: number | undefined;
            let cart: MenuItemModel[] = state.cart;
            const lookingItem = cart.find(cartItem => cartItem.id === item.id);
            if (lookingItem) {
                quantity = lookingItem.quantity;
                quantity ? quantity++ : quantity = 1;
                item = {...item, quantity};
                cart = cart.map(cartItem => cartItem.id === item.id ? item : cartItem);
            } else {
                quantity = 1;
                item = {...item, quantity};
                cart = [...cart, item];
            }
            localStorage.setItem('cart', JSON.stringify(cart));
            return {...state, cart};
        }
    ),
    on(
        ClientActions.removeItemFromCart,
        (state, {item}) => {
            let quantity: number | undefined;
            let cart: MenuItemModel[] = state.cart;
            const lookingItem = cart.find(cartItem => cartItem.id === item.id);
            if (lookingItem) {
                quantity = lookingItem.quantity;
                if (quantity) {
                    if (quantity > 1) {
                        quantity--;
                        item = {...item, quantity};
                        cart = cart.map(cartItem => cartItem.id === item.id ? item : cartItem);
                    } else {
                        cart = cart.filter(cartItem => cartItem.id !== item.id);
                    }
                }
            }
            localStorage.setItem('cart', JSON.stringify(cart));
            return {...state, cart};
        }
    ),
    on(
        ClientActions.toggleCartDisplay,
        (state, {displayCart}) => ({...state, displayCart})
    )
);

export function ClientReducer(state: IClientState | undefined, action: Action) {
    return reducer(state, action);
}
