import {createAction, createActionGroup, emptyProps, props} from '@ngrx/store';
import {MenuItemModel, MenuModel} from '../../models/menu.model';

export const getMenu = createActionGroup({
    source: '[Client] Get Menu',
    events: {
        'Request': emptyProps(),
        'Success': props<{ menu: MenuModel[] }>(),
        'Failure': (error: any) => ({error})
    }
});

export const getCart = createActionGroup({
    source: '[Client] Get Cart',
    events: {
        'Request': emptyProps(),
        'Success': props<{ cart: MenuItemModel[] }>(),
        'Failure': (error: any) => ({error})
    }
});

export const addItemToCart = createAction(
    '[Client] Add Item To Cart',
    props<{ item: MenuItemModel }>()
);

export const removeItemFromCart = createAction(
    '[Client] Remove Item From Cart',
    props<{ item: MenuItemModel }>()
);

export const toggleCartDisplay = createAction(
    '[Client] Toggle Cart Display',
    props<{ displayCart: boolean }>()
);
