import {MenuItemModel, MenuModel} from '../../models/menu.model';

export interface IClientState {
    menu: MenuModel[],
    cart: MenuItemModel[],
    displayCart: boolean
}

export const INITIAL_STATE: IClientState = {
    menu: [],
    cart: [],
    displayCart: false
};

export function getInitialClientState(): IClientState {
    let localStorageCart = JSON.parse(localStorage.getItem('cart') || '[]');
    if (localStorageCart) {
        return {...INITIAL_STATE, cart: localStorageCart}
    }
    return INITIAL_STATE;
}
