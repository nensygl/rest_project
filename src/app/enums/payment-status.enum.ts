export enum PaymentStatusEnum {
    payed = 1,
    awaiting,
    notPayed,
    canceled
}
