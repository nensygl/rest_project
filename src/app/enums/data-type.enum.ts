export enum DataTypeEnum {
    string = 1,
    number,
    image,
    link
}
