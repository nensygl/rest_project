export enum NgrxFeatures {
    login = 'login',
    project = 'project',
    orders = 'orders',
    siteSettings = 'siteSettings',
    client = 'client',
    landing = 'landing'
}
