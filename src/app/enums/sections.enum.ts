export enum SectionsEnum {
    cover = 1,
    buttons,
    menu,
    text,
    banner,
    gallery,
    map,
    contacts
}
