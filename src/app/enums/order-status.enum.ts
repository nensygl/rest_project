export enum OrderStatusEnum {
    created = 1,
    cooking,
    givenToDelivery,
    inDelivery,
    complete,
    canceled
}
