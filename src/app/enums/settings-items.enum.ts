export enum SettingsItemsEnum {
    general = 1,
    colorsAndFonts,
    menuCardsFormat,
    mainPageData,
    header,
    domain,
    paymentSystem,
    userActions,
    subscription
}
