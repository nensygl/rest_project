import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {of} from 'rxjs';
import {projectData} from '../dummy-data/project-data';
import {menuInfo, menuCategoriesList, menuItem, menuItemLong} from '../dummy-data/menu-info';
import {MenuCategoryModel, MenuItemModel} from '../models/menu.model';
import * as _ from 'lodash';
import {updateMenuCategoriesOrder} from '../store/project';

@Injectable()
export class ProjectService {

    constructor(private readonly http: HttpClient) {
    }

    checkIsProjectCreated() {
        console.log('[SERVICE] checkIsProjectCreated()');
        return of(true);
    }

    getProjectBaseData() {
        console.log('[SERVICE] getProjectBaseData()');
        return of(projectData);
    }

    getMenu() {
        console.log('[SERVICE] getMenu()');
        return of(menuInfo);
    }

    getMenuCategoriesList() {
        console.log('[SERVICE] getMenuCategoriesList()');
        return of(menuCategoriesList);
    }

    getMenuItemById(id: number) {
        console.log('[SERVICE] getMenuItemById()');
        return of(menuItem);
    }

    postMenuItem(data: MenuItemModel) {
        console.log('[SERVICE] postMenuItem()');
        return of(menuItemLong);
    }

    deleteMenuItemById(id: number) {
        console.log('[SERVICE] deleteMenuItemById()');
        return of(true);
    }

    deleteMenuItemsList(ids: number[]) {
        console.log('[SERVICE] deleteMenuItemsList()');
        return of(true);
    }

    hideMenuItemsList(ids: number[]) {
        console.log('[SERVICE] hideMenuItemsList()');
        return of(true);
    }

    removeFromStopListItems(ids: number[]) {
        console.log('[SERVICE] removeFromStopListItems()');
        return of(true);
    }

    postCategoryItem(data: MenuCategoryModel) {
        console.log('[SERVICE] postCategoryItem()');
        let result = _.cloneDeep(menuCategoriesList);
        if (!data.id) {
            data.categoryPosition = result.length;
            data.id = result.length;
            result = [...result, data];
        } else {
            result.map(item => data.id === item.id ? data : item);
        }
        return of(result);
    }

    deleteCategoryItem(id: number) {
        console.log('[SERVICE] deleteCategoryItem()', id);
        const updatedList = menuCategoriesList.filter(item => item.id !== id);
        return of(updatedList);
    }

    updateMenuCategoriesOrder(menuCategories: MenuCategoryModel[]) {
        console.log('[SERVICE] updateMenuCategoriesOrder()');
        return of(menuCategories);
    }
}
