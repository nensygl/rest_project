import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {of} from 'rxjs';
import {canceledOrder, ordersList, selectedCookingOrder, selectedInDeliveryOrder, selectedOrder} from '../dummy-data/orders-info';
import {OrderModel, OrdersListFiltersModel} from '../models/orders.model';

@Injectable()
export class OrdersService {

    constructor(private readonly http: HttpClient) {
    }

    getOrdersList(filters: OrdersListFiltersModel) {
        console.log('[ORDERS] getOrdersList()');
        return of(ordersList);
    }

    getOrderInfoById(id: number) {
        console.log('[ORDERS] getOrderInfoById()');
        return of(selectedOrder);
    }

    cancelOrderById(id: number) {
        console.log('[ORDERS] cancelOrderById()');
        return of(canceledOrder);
    }

    updateOrder(order: OrderModel) {
        console.log('[ORDERS] updateOrder()');
        return of(order);
    }
}
