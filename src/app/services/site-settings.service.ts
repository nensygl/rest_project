import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {of} from 'rxjs';
import {
    canceledSubscriptionData,
    colorsAndFontsData,
    fontList,
    generalSettingsData,
    paymentSystemSettings,
    selectedSections,
    siteDomainData, mainPageData, subscriptionData, fontWeightList
} from '../dummy-data/site-settings-info';
import {SelectedSectionModel} from '../models/site-settings.model';
import {
    PaymentSystemSettingsModel,
    SiteColorsAndFontsSettingsModel,
    SiteDomainSettingsModel,
    SiteGeneralSettingsModel, SiteSettingsDataModel
} from '../models/site-general-settings.model';

@Injectable()
export class SiteSettingsService {

    constructor(private readonly http: HttpClient) {
    }

    getSelectedSections() {
        console.log('[SITE SETTINGS] getSelectedSections()');
        return of(selectedSections);
    }

    updateSelectedSections(selectedSections: SelectedSectionModel[]) {
        console.log('[SITE SETTINGS] updateSelectedSections()');
        return of(selectedSections);
    }

    getGeneralSettingsData() {
        console.log('[SITE SETTINGS] getGeneralSettingsData()');
        return of(generalSettingsData);
    }

    updateGeneralSettingsData(generalData: SiteGeneralSettingsModel) {
        console.log('[SITE SETTINGS] updateGeneralSettingsData()');
        return of(generalData);
    }

    getColorsAndFontsData() {
        console.log('[SITE SETTINGS] getColorsAndFontsData()');
        return of(colorsAndFontsData);
    }

    updateColorsAndFontsData(colorAndFonts: SiteColorsAndFontsSettingsModel) {
        console.log('[SITE SETTINGS] updateColorsAndFontsData()');
        return of(colorAndFonts);
    }

    getFontsList() {
        console.log('[SITE SETTINGS] getFontsList()');
        return of(fontList);
    }

    getFontWeightList() {
        console.log('[SITE SETTINGS] getFontWeightList()');
        return of(fontWeightList);
    }

    getSelectedMenuCardsStyle() {
        console.log('[SITE SETTINGS] getSelectedMenuCardsStyle()');
        return of(1);
    }

    updateSelectedMenuCardsStyle(selectedMenuCardsStyle: number) {
        console.log('[SITE SETTINGS] updateSelectedMenuCardsStyle()');
        return of(selectedMenuCardsStyle);
    }

    getMainPageDataData() {
        console.log('[SITE SETTINGS] getMainPageDataData()');
        return of(mainPageData);
    }

    updateMainPageData(mainPageData: any) {
        console.log('[SITE SETTINGS] updateMainPageData()');
        return of(mainPageData);
    }

    getSiteDomainData() {
        console.log('[SITE SETTINGS] getSiteDomainData()');
        return of(siteDomainData);
    }

    updateSiteDomainData(siteDomainData: SiteDomainSettingsModel) {
        console.log('[SITE SETTINGS] updateSiteDomainData()');
        return of(siteDomainData);
    }

    checkIsDomainAvailable(domainName: string) {
        console.log('[SITE SETTINGS] checkIsDomainAvailable()');
        return of(true);
    }

    getPaymentSystemData() {
        console.log('[SITE SETTINGS] getPaymentSystemData()');
        return of(paymentSystemSettings);
    }

    updatePaymentSystemData(paymentSystemSettings: PaymentSystemSettingsModel) {
        console.log('[SITE SETTINGS] updatePaymentSystemData()');
        return of(paymentSystemSettings);
    }

    getSubscriptionData() {
        console.log('[SITE SETTINGS] getSubscriptionData()');
        return of(subscriptionData);
    }

    cancelSubscription() {
        console.log('[SITE SETTINGS] cancelSubscription()');
        return of(canceledSubscriptionData);
    }
}
