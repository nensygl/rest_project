import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {of} from 'rxjs';
import {menuInfo} from '../dummy-data/menu-info';

@Injectable()
export class ClientService {

    constructor(private readonly http: HttpClient) {}

    getMenu() {
        console.log('[CLIENT] getMenu()');
        return of(menuInfo);
    }

}
