import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {of} from 'rxjs';

@Injectable()
export class LoginService {

    constructor(private readonly http: HttpClient) {
    }

    getSMSCode(phoneNum: string) {
        console.log('[LOGIN] getSMSCode()');
        /*const path = `192.168.0.99/api/login/getCode=${phoneNum}`
        return this.http.get(path);*/
        return of(true);
    }

    checkSMSCode(code: string) {
        console.log('[LOGIN] checkSMSCode()');
        /*const path = `192.168.0.99/api/login/checkCode=${code}`
        return this.http.get(path);*/
        const result: boolean = '1234' === code;
        return of(result);
    }
}
