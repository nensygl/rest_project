import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {CallbackFormModel} from '../models/landing.model';
import {of} from 'rxjs';

@Injectable()
export class LandingService {

    constructor(private readonly http: HttpClient) {
    }

    postCallbackForm(callbackForm: CallbackFormModel) {
        console.log('[LANDING] postCallbackForm()');
        return of(true);
    }
}
