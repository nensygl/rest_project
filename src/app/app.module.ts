import { NgDompurifySanitizer } from "@tinkoff/ng-dompurify";
import {
    TuiRootModule,
    TuiDialogModule,
    TuiAlertModule,
    TUI_SANITIZER,
    TuiErrorModule,
    TuiTextfieldControllerModule,
    TuiDataListModule, TuiLoaderModule, TuiCalendarModule, TuiHintModule, TuiSvgModule
} from '@taiga-ui/core';
import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {LandingComponent} from './components/landing/landing.component';
import {LandingHeaderComponent} from './components/landing-header/landing-header.component';
import {DevNavComponent} from './components/dev-nav/dev-nav.component';
import {NoopAnimationsModule} from '@angular/platform-browser/animations';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {ReactiveFormsModule, FormsModule} from '@angular/forms';
import {NgOptimizedImage, registerLocaleData} from '@angular/common';
import en from '@angular/common/locales/en';
import {HttpClientModule} from '@angular/common/http';
import {LandingFooterComponent} from './components/landing-footer/landing-footer.component';
import {StoreModule} from '@ngrx/store';
import {EffectsModule} from '@ngrx/effects';
import {StoreDevtoolsModule} from '@ngrx/store-devtools';
import {AppReducer, metaReducers} from './store';
import {LoginEffects} from './store/login';
import {ClientService, LandingService, LoginService, OrdersService, ProjectService, SiteSettingsService} from './services';
import {PhoneMaskDirective} from './directives/phone-mask.directive';
import {CodeInputModule} from 'angular-code-input';
import {UserHeaderComponent} from './components/user-header/user-header.component';
import {ProjectMainPageComponent} from './components/project-main-page/project-main-page.component';
import {ProjectEffects} from './store/project';
import {EditMenuComponent} from './components/edit-menu/edit-menu.component';
import {AddNewItemCardComponent} from './components/add-new-item-card/add-new-item-card.component';
import {AddEditItemComponent} from './components/add-edit-item/add-edit-item.component';
import {AddEditMenuCategoryComponent} from './components/add-edit-menu-category/add-edit-menu-category.component';
import {StopListComponent} from './components/stop-list/stop-list.component';
import {OrdersComponent} from './components/orders/orders.component';
import {OrdersEffects} from './store/orders';
import {SiteSettingsEffects} from './store/site-settings';
import { SiteSectionsSettingsComponent } from './components/site-sections-settings/site-sections-settings.component';
import { SiteGeneralSettingsComponent } from './components/site-general-settings/site-general-settings.component';
import { CoverV1Component } from './components/site-constructor-components/cover/cover-v1/cover-v1.component';
import { CoverV2Component } from './components/site-constructor-components/cover/cover-v2/cover-v2.component';
import { MenuV1Component } from './components/site-constructor-components/menu/menu-v1/menu-v1.component';
import { TextV1Component } from './components/site-constructor-components/text/text-v1/text-v1.component';
import { TextV2Component } from './components/site-constructor-components/text/text-v2/text-v2.component';
import { TextV3Component } from './components/site-constructor-components/text/text-v3/text-v3.component';
import { TextV4Component } from './components/site-constructor-components/text/text-v4/text-v4.component';
import { BannerV1Component } from './components/site-constructor-components/banner/banner-v1/banner-v1.component';
import { BannerV2Component } from './components/site-constructor-components/banner/banner-v2/banner-v2.component';
import { GalleryV1Component } from './components/site-constructor-components/gallery/gallery-v1/gallery-v1.component';
import { GalleryV2Component } from './components/site-constructor-components/gallery/gallery-v2/gallery-v2.component';
import { MapV1Component } from './components/site-constructor-components/map/map-v1/map-v1.component';
import { MapV2Component } from './components/site-constructor-components/map/map-v2/map-v2.component';
import { ContactsV1Component } from './components/site-constructor-components/contacts/contacts-v1/contacts-v1.component';
import { ButtonsV1Component } from './components/site-constructor-components/buttons/buttons-v1/buttons-v1.component';
import { ButtonsV2Component } from './components/site-constructor-components/buttons/buttons-v2/buttons-v2.component';
import { SitePreviewComponent } from './components/site-preview/site-preview.component';
import { GeneralComponent } from './components/site-general-settings-components/general/general.component';
import { ColorAndFontsComponent } from './components/site-general-settings-components/color-and-fonts/color-and-fonts.component';
import { MenuCardsFormatComponent } from './components/site-general-settings-components/menu-cards-format/menu-cards-format.component';
import { MainPageDataComponent } from './components/site-general-settings-components/main-page-data/main-page-data.component';
import { HeaderComponent } from './components/site-general-settings-components/header/header.component';
import { DomainComponent } from './components/site-general-settings-components/domain/domain.component';
import { PaymentSystemComponent } from './components/site-general-settings-components/payment-system/payment-system.component';
import { UserActionsComponent } from './components/site-general-settings-components/user-actions/user-actions.component';
import { SubscriptionComponent } from './components/site-general-settings-components/subscription/subscription.component';
import {ColorPickerModule} from 'ngx-color-picker';
import { ClientMenuComponent } from './components/client-menu/client-menu.component';
import { ClientHeaderComponent } from './components/client-header/client-header.component';
import {ClientEffects} from './store/client';
import { ClientCartComponent } from './components/client-cart/client-cart.component';
import { ClientCartHeaderComponent } from './components/client-cart-header/client-cart-header.component';
import { ConstructorDevToMobComponent } from './components/constructor-dev-to-mob/constructor-dev-to-mob.component';
import {
    TuiCheckboxLabeledModule,
    TuiDataListWrapperModule,
    TuiFieldErrorPipeModule, TuiInputCopyModule,
    TuiInputModule, TuiInputNumberModule,
    TuiInputPhoneInternationalModule, TuiSelectModule,
    TuiSortCountriesPipeModule,
    TuiTextAreaModule, TuiTilesModule
} from '@taiga-ui/kit';
import {LandingEffects} from './store/landing';
import {TuiForModule, TuiLetModule} from '@taiga-ui/cdk';
import {TuiInputColorModule} from '@taiga-ui/addon-editor';

registerLocaleData(en);

@NgModule({
    declarations: [
        AppComponent,
        LandingComponent,
        LandingHeaderComponent,
        DevNavComponent,
        LandingFooterComponent,
        PhoneMaskDirective,
        UserHeaderComponent,
        ProjectMainPageComponent,
        EditMenuComponent,
        AddNewItemCardComponent,
        AddEditItemComponent,
        AddEditMenuCategoryComponent,
        StopListComponent,
        OrdersComponent,
        SiteSectionsSettingsComponent,
        SiteGeneralSettingsComponent,
        CoverV1Component,
        CoverV2Component,
        MenuV1Component,
        TextV1Component,
        TextV2Component,
        TextV3Component,
        TextV4Component,
        BannerV1Component,
        BannerV2Component,
        GalleryV1Component,
        GalleryV2Component,
        MapV1Component,
        MapV2Component,
        ContactsV1Component,
        ButtonsV1Component,
        ButtonsV2Component,
        SitePreviewComponent,
        GeneralComponent,
        ColorAndFontsComponent,
        MenuCardsFormatComponent,
        MainPageDataComponent,
        HeaderComponent,
        DomainComponent,
        PaymentSystemComponent,
        UserActionsComponent,
        SubscriptionComponent,
        ClientMenuComponent,
        ClientHeaderComponent,
        ClientCartComponent,
        ClientCartHeaderComponent,
        ConstructorDevToMobComponent
    ],
    imports: [
        BrowserModule,
        AppRoutingModule,
        NoopAnimationsModule,
        BrowserAnimationsModule,
        ReactiveFormsModule,
        FormsModule,
        HttpClientModule,
        StoreModule.forRoot(AppReducer, {metaReducers}),
        EffectsModule.forRoot([
            LoginEffects,
            ProjectEffects,
            OrdersEffects,
            SiteSettingsEffects,
            ClientEffects,
            LandingEffects
        ]),
        StoreDevtoolsModule.instrument({
            maxAge: 25
        }),
        CodeInputModule,
        ColorPickerModule,
        TuiRootModule,
        TuiDialogModule,
        TuiAlertModule,
        TuiInputModule,
        TuiInputPhoneInternationalModule,
        TuiSortCountriesPipeModule,
        TuiErrorModule,
        TuiFieldErrorPipeModule,
        TuiTextAreaModule,
        TuiTextfieldControllerModule,
        TuiDataListWrapperModule,
        TuiSelectModule,
        TuiDataListModule,
        TuiLoaderModule,
        TuiLetModule,
        TuiInputNumberModule,
        TuiInputColorModule,
        TuiForModule,
        TuiInputCopyModule,
        TuiCalendarModule,
        TuiCheckboxLabeledModule,
        TuiTilesModule,
        NgOptimizedImage,
        TuiHintModule,
        TuiSvgModule
    ],
    providers: [
        LoginService,
        ProjectService,
        OrdersService,
        SiteSettingsService,
        ClientService,
        LandingService,
        {provide: TUI_SANITIZER, useClass: NgDompurifySanitizer}
    ],
    bootstrap: [AppComponent]
})
export class AppModule {
}
