import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {TuiCountryIsoCode} from '@taiga-ui/i18n';
import {TUI_VALIDATION_ERRORS} from '@taiga-ui/kit';
import {CallbackFormModel} from '../../models/landing.model';
import {Store} from '@ngrx/store';
import {AppState} from '../../store';
import {Observable} from 'rxjs';
import {LandingSelectors} from '../../store/landing';
import * as LandingActions from '../../store/landing';

@Component({
    selector: 'app-landing',
    templateUrl: './landing.component.html',
    styleUrls: ['./landing.component.less'],
    providers: [
        {
            provide: TUI_VALIDATION_ERRORS,
            useValue: {
                required: 'Field should not be empty!',
                email: 'Enter valid email'
            }
        }
    ]
})
export class LandingComponent implements OnInit {

    callbackForm: FormGroup;
    readonly countries: Array<TuiCountryIsoCode> = Object.values(TuiCountryIsoCode);
    countryIsoCode = TuiCountryIsoCode.US;
    isFormSendSuccess: Observable<boolean>;

    constructor(
        private readonly store: Store<AppState>,
        private readonly formBuilder: FormBuilder
    ) {
        this.callbackForm = this.formBuilder.group({
            'userName': new FormControl('', Validators.required),
            'userEmail': new FormControl('', [Validators.required, Validators.email]),
            'userPhone': new FormControl('', [Validators.required, Validators.minLength(12)])
        });
    }

    ngOnInit(): void {
        this.isFormSendSuccess = this.store.select(LandingSelectors.getIsFormSendSuccess);
    }

    onCallbackFormSubmit() {
        if (this.callbackForm.valid) {
            const callbackForm: CallbackFormModel = this.callbackForm.getRawValue();
            console.log('callbackForm', callbackForm);
            this.store.dispatch(LandingActions.postCallbackForm.request({callbackForm}));
        }
    }

}
