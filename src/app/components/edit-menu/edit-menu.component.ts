import {ChangeDetectionStrategy, Component, OnInit} from '@angular/core';
import {Store} from '@ngrx/store';
import {AppState} from '../../store';
import {ActivatedRoute, Router} from '@angular/router';
import {MenuItemModel, MenuModel} from '../../models/menu.model';
import * as ProjectActions from '../../store/project/project.actions';
import {ProjectSelectors} from '../../store/project';
import * as _ from 'lodash';

@Component({
    selector: 'app-edit-menu',
    templateUrl: './edit-menu.component.html',
    styleUrls: ['./edit-menu.component.less'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class EditMenuComponent implements OnInit {
    tableMode: boolean = true;
    menu: MenuModel[];
    menuItemsList: MenuItemModel[];
    displayDeleteModal: boolean = false;
    displayDeleteListModal: boolean = false;
    displayHideListModal: boolean = false;
    idToDelete: number;
    isGridSelectionActive: boolean = false;
    selectedItemsIds: number[] = [];

    constructor(
        private readonly store: Store<AppState>,
        private readonly router: Router,
        private readonly route: ActivatedRoute
    ) {
        this.store.select(ProjectSelectors.getMenu).subscribe(
            menu => {
                if (menu) {
                    this.menu = menu;
                    let menuItems: MenuItemModel[] = [];
                    menu.map(menuCats => menuItems = menuItems.concat(menuCats.menuItems));
                    this.menuItemsList = menuItems;
                }
            }
        );
    }

    ngOnInit(): void {
        this.store.dispatch(ProjectActions.getMenu.request());
    }

    editMenuItem(id: number) {
        this.router.navigate([`edit/${id}`], {relativeTo: this.route});
    }

    addMenuItem() {
        this.router.navigate(['new'], {relativeTo: this.route});
    }

    displayDeleteItemModal(id: number) {
        this.idToDelete = id;
        this.displayDeleteModal = true;
    }

    deleteMenuItem() {
        this.store.dispatch(ProjectActions.deleteMenuItemById.request({id: this.idToDelete}));
        setTimeout(() => {
            this.displayDeleteModal = false;
        }, 1000);
    }

    deleteMenuItemsList() {
        this.store.dispatch(ProjectActions.deleteMenuItemsList.request({ids: this.selectedItemsIds}));
        setTimeout(() => {
            this.displayDeleteListModal = false;
            this.isGridSelectionActive = false;
        }, 1000);
    }

    hideMenuItemsList() {
        this.store.dispatch(ProjectActions.hideMenuItemsList.request({ids: this.selectedItemsIds}));
        setTimeout(() => {
            this.displayHideListModal = false;
            this.isGridSelectionActive = false;
        }, 1000);
    }

    toggleItemVisibility(menuItem: MenuItemModel) {
        let item = _.clone(menuItem);
        item.isHidden = !item.isHidden;
        this.store.dispatch(ProjectActions.postMenuItem.request({menuItem: item}));
    }

    getCategoryName(categoryId: number) {
        const category = this.menu.find(catItem => catItem.id === categoryId);
        if (category) {
            return category.categoryName;
        }
        return '';
    }

    toggleViewMode() {
        this.tableMode = !this.tableMode;
    }

    openStopList() {
        this.router.navigate(['../stop-list'], {relativeTo: this.route});
    }

    toggleGridSelection() {
        this.isGridSelectionActive = !this.isGridSelectionActive;
        this.selectedItemsIds = [];
    }

    getModalHeaderWithCounter(isDelete: boolean) {
        if (isDelete) {
            return `Удалить все выбранные позиции? (${this.selectedItemsIds.length})`;
        }
        return `Добавить все выбранные позиции в стоп-лист? (${this.selectedItemsIds.length})`;
    }

    selectItem(id: number) {
        this.selectedItemsIds.push(id);
    }

    unselectItem(id: number) {
        let index = this.selectedItemsIds.indexOf(id);
        if (index > -1) {
            this.selectedItemsIds.splice(index, 1);
        }
    }
}
