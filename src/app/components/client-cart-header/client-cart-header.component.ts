import {Component, HostListener, OnDestroy, OnInit} from '@angular/core';
import {Observable} from 'rxjs';
import {MenuItemModel} from '../../models/menu.model';
import {Store} from '@ngrx/store';
import {AppState} from '../../store';
import {ClientSelectors} from '../../store/client';
import * as ClientActions from '../../store/client';
import {UntilDestroy, untilDestroyed} from '@ngneat/until-destroy';

@UntilDestroy()
@Component({
    selector: 'app-client-cart-header',
    templateUrl: './client-cart-header.component.html',
    styleUrls: ['./client-cart-header.component.less']
})
export class ClientCartHeaderComponent implements OnInit, OnDestroy {
    isCartFixed: boolean = false;
    cart: Observable<MenuItemModel[]>;
    cartLength: number;
    isItemsInCart: boolean = false;

    constructor(
        private readonly store: Store<AppState>
    ) {
        this.store.select(ClientSelectors.getCart)
            .pipe(untilDestroyed(this))
            .subscribe(cart => {
                let length = 0;
                cart.map(item => length += item.quantity || 0);
                this.cartLength = length;
                this.isItemsInCart = length > 0;
            });
    }

    ngOnInit(): void {
        //
    }

    ngOnDestroy() {
        //
    }

    @HostListener('window:scroll', ['$event']) onscroll() {
        this.isCartFixed = window.scrollY > 80;
    }

    showCart() {
        this.store.dispatch(ClientActions.toggleCartDisplay({displayCart: true}));
    }
}
