import {Component, OnInit} from '@angular/core';

@Component({
    selector: 'app-client-header',
    templateUrl: './client-header.component.html',
    styleUrls: ['./client-header.component.less']
})
export class ClientHeaderComponent implements OnInit {
    displayMenu: boolean = false;
    displayLoginHint: boolean = false;

    constructor() {
    }

    ngOnInit(): void {
    }

    toggleOpenMenu() {
        this.displayMenu = !this.displayMenu;
    }

    toggleDisplayLoginHint() {
        this.displayLoginHint = !this.displayLoginHint;
    }

}
