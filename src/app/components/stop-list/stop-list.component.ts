import {ChangeDetectionStrategy, Component, OnInit} from '@angular/core';
import {MenuItemModel, MenuModel} from '../../models/menu.model';
import {Store} from '@ngrx/store';
import {AppState} from '../../store';
import {ActivatedRoute, Router} from '@angular/router';
import {ProjectSelectors} from '../../store/project';
import * as ProjectActions from '../../store/project/project.actions';
import * as _ from 'lodash';

@Component({
    selector: 'app-stop-list',
    templateUrl: './stop-list.component.html',
    styleUrls: ['./stop-list.component.less'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class StopListComponent implements OnInit {
    menu: MenuModel[];
    stopListItemsList: MenuItemModel[];
    displayDeleteModal: boolean = false;
    displayRemoveFromStopModal: boolean = false;
    idToDelete: number;

    constructor(
        private readonly store: Store<AppState>,
        private readonly router: Router,
        private readonly route: ActivatedRoute
    ) {
        this.store.select(ProjectSelectors.getMenu).subscribe(
            menu => {
                if (menu) {
                    this.menu = menu;
                    let menuItems: MenuItemModel[] = [];
                    menu.map(menuCats => menuItems = menuItems.concat(menuCats.menuItems));
                    this.stopListItemsList = menuItems.filter(item => item.isHidden);
                }
            }
        );
    }

    ngOnInit(): void {
        this.store.dispatch(ProjectActions.getMenu.request());
    }

    getCategoryName(categoryId: number) {
        const category = this.menu.find(catItem => catItem.id === categoryId);
        if (category) {
            return category.categoryName;
        }
        return '';
    }

    editMenuItem(id: number) {
        this.router.navigate([`../menu/edit/${id}`], {relativeTo: this.route});
    }

    displayDeleteItemModal(id: number) {
        this.idToDelete = id;
        this.displayDeleteModal = true;
    }

    deleteMenuItem() {
        this.store.dispatch(ProjectActions.deleteMenuItemById.request({id: this.idToDelete}));
        setTimeout(() => {
            this.displayDeleteModal = false;
        }, 1000);
    }

    toggleItemVisibility(menuItem: MenuItemModel) {
        let item = _.clone(menuItem);
        item.isHidden = !item.isHidden;
        this.store.dispatch(ProjectActions.postMenuItem.request({menuItem: item}));
    }

    goToMenu() {
        this.router.navigate(['../menu'], {relativeTo: this.route});
    }

    removeAllFromStopList() {
        let ids = this.stopListItemsList.map(item => item.id);
        this.store.dispatch(ProjectActions.removeFromStopListItems.request({ids}));
    }
}
