import {ChangeDetectionStrategy, Component, OnInit} from '@angular/core';
import {MenuCategoryModel} from '../../models/menu.model';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {Store} from '@ngrx/store';
import {AppState} from '../../store';
import {ActivatedRoute, Router} from '@angular/router';
import {MENU_ITEM_MAX_LEN} from '../../utils/constants';
import {ProjectSelectors} from '../../store/project';
import * as ProjectActions from '../../store/project/project.actions';
import {TUI_VALIDATION_ERRORS} from '@taiga-ui/kit';
import {maxLengthValidator, minLengthValidator} from '../../utils/functions';
import * as _ from 'lodash';

@Component({
    selector: 'app-add-edit-menu-category',
    templateUrl: './add-edit-menu-category.component.html',
    styleUrls: ['./add-edit-menu-category.component.less'],
    changeDetection: ChangeDetectionStrategy.OnPush,
    providers: [
        {
            provide: TUI_VALIDATION_ERRORS,
            useValue: {
                required: 'Поле обязаельно для заполнения',
                maxlength: maxLengthValidator,
                minlength: minLengthValidator,
            }
        }
    ]
})
export class AddEditMenuCategoryComponent implements OnInit {

    isAddNew: boolean = true;
    categoryItem: MenuCategoryModel = new MenuCategoryModel;
    menuCategories: MenuCategoryModel[];
    categoryForm: FormGroup;
    activeItemId: number = 0;
    displayModal: boolean = false;
    idToDelete: number;
    order: Map<number, number> = new Map();
    isUpdateOrderBtnDisabled: boolean = true;

    constructor(
        private readonly store: Store<AppState>,
        private readonly router: Router,
        private readonly route: ActivatedRoute
    ) {
        this.categoryForm = new FormGroup({
            categoryName: new FormControl('', [Validators.required, Validators.maxLength(MENU_ITEM_MAX_LEN.CATEGORY_NAME)]),
            // categoryPosition: new FormControl('', Validators.required)
        });
        this.store.select(ProjectSelectors.getMenuCategories).subscribe(
            menuCategories => {
                if (menuCategories) {
                    this.menuCategories = menuCategories;
                }
            }
        );
    }

    ngOnInit(): void {
        this.store.dispatch(ProjectActions.getMenuCategoriesList.request());
    }

    fillForm(item: MenuCategoryModel) {
        this.categoryForm.patchValue({
            categoryName: item.categoryName,
            // categoryPosition: item.categoryPosition
        });
    }

    resetForm() {
        this.fillForm(this.categoryItem);
    }

    submitForm() {
        console.log('submit form called');
        if (this.categoryForm.valid) {
            console.log('category form valid');
            const categoryItem: MenuCategoryModel = this.categoryForm.getRawValue();
            categoryItem.id = this.activeItemId;
            console.log(categoryItem);
            this.store.dispatch(ProjectActions.postCategoryItem.request({categoryItem}));
        }
    }

    getActiveCategoryItemInfo(id: number) {
        console.log('order', this.order);
        this.activeItemId = id;
        const res = this.menuCategories.find(cat => cat.id === id);
        if (res) {
            this.categoryItem = res;
            this.fillForm(res);
        }
    }

    showModal(id: number) {
        console.log('show modal idToDelete', id);
        this.displayModal = true;
        this.idToDelete = id;
    }

    deleteCategory() {
        console.log('delete category idToDelete', this.idToDelete);
        this.store.dispatch(ProjectActions.deleteCategoryItemById.request({id: this.idToDelete}));
        setTimeout(() => {
            this.displayModal = false;
        }, 1000);
    }

    updateCategoriesOrder() {
        console.log('order change', this.order);
        let categories = _.cloneDeep(this.menuCategories);
        categories.map(cat => {
            const newPosition = this.order.get(cat.categoryPosition);
            if (!_.isUndefined(newPosition)) {
                cat.categoryPosition = newPosition;
            }
        });
        this.store.dispatch(ProjectActions.updateMenuCategoriesOrder.request({ menuCategories: categories}));
        // this.menuCategories = categories;
        console.log('menuCategories updated', categories);
    }

    returnToMenu() {
        this.router.navigate(['../'], {relativeTo: this.route});
    }

    unlockUpdateOrderBtn() {
        this.isUpdateOrderBtnDisabled = false;
    }
}
