import {ChangeDetectionStrategy, Component, OnInit} from '@angular/core';
import {Observable} from 'rxjs';
import {OrderListModel, OrderModel, OrdersListFiltersModel} from '../../models/orders.model';
import {Store} from '@ngrx/store';
import {AppState} from '../../store';
import {ActivatedRoute, Router} from '@angular/router';
import * as OrdersActions from '../../store/orders/orders.actions';
import {OrdersSelectors} from '../../store/orders';
import {OrderStatusEnum} from '../../enums/order-status.enum';
import {PaymentStatusEnum} from '../../enums/payment-status.enum';
import {selectedOrder} from '../../dummy-data/orders-info';
import * as _ from 'lodash';
import {TuiDay} from '@taiga-ui/cdk';

@Component({
    selector: 'app-orders',
    templateUrl: './orders.component.html',
    styleUrls: ['./orders.component.less'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class OrdersComponent implements OnInit {
    orderList$: Observable<OrderListModel[]>;
    selectedOrder: OrderModel;
    displaySelectedOrderLoading: boolean = false;
    activeOrderId: number = 4;
    statusList = OrderStatusEnum;
    paymentStatus = PaymentStatusEnum;
    displayCancelModal: boolean = false;
    displayFilters: boolean = false;
    displayCalendar: boolean = false;
    ordersListFilters: OrdersListFiltersModel = new OrdersListFiltersModel();

    constructor(
        private readonly store: Store<AppState>,
        private readonly router: Router,
        private readonly route: ActivatedRoute
    ) {
        this.orderList$ = this.store.select(OrdersSelectors.getOrdersList);
        this.getSelectedOrder(4);
        this.store.select(OrdersSelectors.getSelectedOrder)
            .subscribe(order => {
                if (order) {
                    this.selectedOrder = order;
                }
            });
    }

    ngOnInit(): void {
        this.fillFilters();
        this.getOrdersList();
    }

    getOrdersList() {
        this.store.dispatch(OrdersActions.getOrdersList.request({ filters: this.ordersListFilters }));
    }

    getSelectedOrder(id: number) {
        this.activeOrderId = id;
        this.store.dispatch(OrdersActions.getOrderById.request({ id }));
        this.displaySelectedOrderLoading = true;
    }

    fillFilters() {
        this.ordersListFilters.filters = [
            {
                label: 'Создан',
                value: this.statusList.created,
                checked: true
            },
            {
                label: 'Готовят',
                value: this.statusList.cooking,
                checked: true
            },
            {
                label: 'Передали в доставку',
                value: this.statusList.givenToDelivery,
                checked: true
            },
            {
                label: 'В пути',
                value: this.statusList.inDelivery,
                checked: true
            },
            {
                label: 'Завершен',
                value: this.statusList.complete,
                checked: true
            },
            {
                label: 'Отменен',
                value: this.statusList.canceled,
                checked: true
            }
        ];
        this.ordersListFilters.date = TuiDay.currentLocal();
    }

    getDate(date: Date) {
        let day = date.getDate().toString();
        if (day.length < 2) {
            day = '0' + day;
        }
        let month = (date.getMonth() + 1).toString();
        if (month.length < 2) {
            month = '0' + month;
        }
        let year = date.getFullYear();
        return `${day}.${month}.${year}`;
    }

    getTime(date: Date) {
        let hours = date.getHours().toString();
        if (hours.length < 2) {
            hours = '0' + hours;
        }
        let minutes = date.getMinutes().toString();
        if (minutes.length < 2) {
            minutes = '0' + minutes;
        }
        return `${hours}:${minutes}`;
    }

    getOrderStatusIcon(orderStatus: number) {
        let result = '';
        switch (orderStatus) {
            case 1:
                result = './assets/imgs/purple-status.svg';
                break;
            case 2:
            case 3:
            case 4:
                result = './assets/imgs/yellow-status.svg';
                break;
            case 5:
                result = './assets/imgs/green-status.svg';
                break;
            case 6:
                result = './assets/imgs/red-status.svg';
                break;
            default:
                result = './assets/imgs/yellow-status.svg';
        }
        return result;
    }

    getLastUpdateTime() {
        const index = this.selectedOrder.statusHistory.length - 1;
        const lastUpdateTime = this.selectedOrder.statusHistory[index];
        return this.getTime(lastUpdateTime.statusChangeTime);
    }

    nextStep() {
        let order = _.cloneDeep(this.selectedOrder);
        const lastIndex = order.statusHistory.length - 1;
        let lastStep = order.statusHistory[lastIndex];
        order.statusHistory.push({status: (lastStep.status + 1), statusChangeTime: new Date()});
        order.orderStatus++;
        this.store.dispatch(OrdersActions.updateOrder.request({selectedOrder: order}))
    }

    isOrderStatusDone(status: number) {
        const index = this.selectedOrder.statusHistory.findIndex(item => item.status === status);
        return index !== -1;
    }

    getStatusTime(status: OrderStatusEnum) {
        const index = this.selectedOrder.statusHistory.findIndex(item => item.status === status);
        if (index !== -1) {
            return this.getTime(this.selectedOrder.statusHistory[index].statusChangeTime);
        }
        return '';
    }

    getPaymentMethodName(paymentMethod: number) {
        let result: string = '';
        switch (paymentMethod) {
            case 1:
                result = 'Оплата картой';
                break;
            case 2:
                result = 'Оплата наличными';
                break;
            default:
                result = 'Способ не указан';
        }
        return result;
    }

    cancelOrder() {
        this.store.dispatch(OrdersActions.cancelOrder.request({id: selectedOrder.orderId}));
        setTimeout(() => {
            this.displayCancelModal = false;
        }, 1000);
    }

    toggleFilters() {
        this.displayFilters = !this.displayFilters;
        this.displayCalendar = false;
    }

    toggleCalendar() {
        this.displayCalendar = !this.displayCalendar;
        this.displayFilters = false;
    }

    updateFilters(checked: boolean, value: number) {
        const filters: OrdersListFiltersModel = _.cloneDeep(this.ordersListFilters);
        filters.filters.map(fil => {
            if (fil.value === value) fil.checked = checked;
        });
        this.ordersListFilters = filters;
        this.getOrdersList();
    }

    updateCalendar(date: TuiDay) {
        const filters = _.clone(this.ordersListFilters);
        filters.date = date;
        this.ordersListFilters = filters;
        this.getOrdersList();
    }
}
