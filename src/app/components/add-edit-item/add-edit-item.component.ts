import {ChangeDetectionStrategy, Component, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {Store} from '@ngrx/store';
import {AppState} from '../../store';
import * as ProjectActions from '../../store/project/project.actions';
import {ProjectSelectors} from '../../store/project';
import {MenuCategoryModel, MenuItemModel} from '../../models/menu.model';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {MENU_ITEM_MAX_LEN} from '../../utils/constants';
import {UntilDestroy, untilDestroyed} from '@ngneat/until-destroy';
import {TuiContextWithImplicit, tuiPure, TuiStringHandler} from '@taiga-ui/cdk';
import {TUI_VALIDATION_ERRORS} from '@taiga-ui/kit';
import {maxLengthValidator, minLengthValidator} from '../../utils/functions';

@UntilDestroy()
@Component({
    selector: 'app-add-edit-item',
    templateUrl: './add-edit-item.component.html',
    styleUrls: ['./add-edit-item.component.less'],
    changeDetection: ChangeDetectionStrategy.OnPush,
    providers: [
        {
            provide: TUI_VALIDATION_ERRORS,
            useValue: {
                required: 'Поле обязаельно для заполнения',
                maxlength: maxLengthValidator,
                minlength: minLengthValidator,
            }
        }
    ]
})
export class AddEditItemComponent implements OnInit, OnDestroy {

    isAddNew: boolean = false;
    menuItem: MenuItemModel;
    menuCategories: MenuCategoryModel[];
    itemForm: FormGroup;
    menuItemsList: MenuItemModel[] = [];
    activeItemId: number;

    constructor(
        private readonly store: Store<AppState>,
        private readonly router: Router,
        private readonly route: ActivatedRoute
    ) {
        this.itemForm = new FormGroup({
            itemName: new FormControl('', [Validators.required, Validators.maxLength(MENU_ITEM_MAX_LEN.NAME)]),
            itemPrice: new FormControl('', Validators.required),
            description: new FormControl('', Validators.maxLength(MENU_ITEM_MAX_LEN.DESCRIPTION)),
            categoryId: new FormControl('', Validators.required)
        });
        this.store.select(ProjectSelectors.getMenuItem)
            .pipe(untilDestroyed(this))
            .subscribe(
                menuItem => {
                    this.menuItem = menuItem;
                    this.fillForm(menuItem);
                    this.activeItemId = menuItem.id;
                    console.log('activeId', this.activeItemId);
                }
            );
        this.store.select(ProjectSelectors.getMenuCategories)
            .pipe(untilDestroyed(this))
            .subscribe(menuCategories => {
                if (menuCategories) {
                    this.menuCategories = menuCategories;
                } else {
                    this.store.dispatch(ProjectActions.getMenuCategoriesList.request());
                }
            });
        this.store.select(ProjectSelectors.getMenu)
            .pipe(untilDestroyed(this))
            .subscribe(
                categoryList => {
                    if (categoryList) {
                        for (let category of categoryList) {
                            this.menuItemsList = this.menuItemsList.concat(category.menuItems);
                        }
                    }
                }
            );
    }

    ngOnInit(): void {
        this.activeItemId = Number(this.route.snapshot.paramMap.get('id'));
        if (this.activeItemId !== 0) {
            this.isAddNew = false;
            this.store.dispatch(ProjectActions.getMenuItemById.request({id: this.activeItemId}));
        } else {
            this.isAddNew = true;
        }
        this.store.dispatch(ProjectActions.getMenu.request());
    }

    @tuiPure
    stringifyMenuCategories(
        menuCategories: readonly MenuCategoryModel[],
    ): TuiStringHandler<TuiContextWithImplicit<number>> {
        const map = new Map(menuCategories.map(({id, categoryName}) => [id, categoryName] as [number, string]));

        return ({$implicit}: TuiContextWithImplicit<number>) => map.get($implicit) || '';
    }

    fillForm(item: MenuItemModel) {
        this.itemForm.patchValue({
            itemName: item.itemName,
            itemPrice: item.itemPrice,
            description: item.description,
            categoryId: item.categoryId
        });
    }

    resetForm() {
        this.fillForm(this.menuItem);
    }

    submitForm() {
        if (this.itemForm.valid) {
            const menuItem: MenuItemModel = this.itemForm.getRawValue();
            menuItem.id = this.activeItemId;
            console.log(menuItem);
            this.store.dispatch(ProjectActions.postMenuItem.request({menuItem}));
        }
    }

    loadItemInfo(id: number) {
        console.log('item with id', id, 'clicked');
        this.store.dispatch(ProjectActions.getMenuItemById.request({id}));
    }

/*    isFormInvalid() {
        let isTouched = this.itemForm.controls['itemName'].touched
            && this.itemForm.controls['itemPrice'].touched
            && this.itemForm.controls['categoryId'].touched;
        return !this.itemForm.valid && isTouched;
    }

    isNameLenValid() {
        return this.itemForm.get('itemName')?.hasError('maxlength');
    }

    isDescLenValid() {
        return this.itemForm.get('description')?.hasError('maxlength');
    }

    getNameCounter() {
        const fieldVal = this.itemForm.get('itemName')?.value;
        return `${fieldVal.length}/${MENU_ITEM_MAX_LEN.NAME}`;
    }*/
/*
    getDescriptionCounter() {
        const fieldVal = this.itemForm.get('description')?.value;
        return `${fieldVal.length}/${MENU_ITEM_MAX_LEN.DESCRIPTION}`;
    }*/

    returnToMenu() {
        this.router.navigate(['../../'], {relativeTo: this.route});
    }

    ngOnDestroy() {
        //
    }
}
