import {Component, OnDestroy, OnInit} from '@angular/core';
import {Store} from '@ngrx/store';
import {AppState} from '../../store';
import {ActivatedRoute, Router} from '@angular/router';
import {SiteSettingsSelectors} from '../../store/site-settings';
import * as SiteSettingsActions from '../../store/site-settings';
import {SectionsEnum} from '../../enums/sections.enum';
import {SelectedSectionModel} from '../../models/site-settings.model';
import {UntilDestroy, untilDestroyed} from '@ngneat/until-destroy';

@UntilDestroy()
@Component({
    selector: 'app-site-preview',
    templateUrl: './site-preview.component.html',
    styleUrls: ['./site-preview.component.less']
})
export class SitePreviewComponent implements OnInit, OnDestroy {
    siteSectionsEnum = SectionsEnum;
    selectedSections: SelectedSectionModel[];
    isDesktopView: boolean = true;

    constructor(
        private readonly store: Store<AppState>,
        private readonly router: Router,
        private readonly route: ActivatedRoute
    ) {
        this.store.select(SiteSettingsSelectors.getSelectedSections)
            .pipe(untilDestroyed(this))
            .subscribe(sections => this.selectedSections = sections);
    }

    ngOnInit(): void {
        this.store.dispatch(SiteSettingsActions.getSelectedSections.request());
    }

    getActiveSectionId(sectionId: number) {
        let activeItemId = 0;
        const index = this.selectedSections.findIndex(item => item.sectionId === sectionId);
        if (index !== -1) {
            this.selectedSections[index].selectionInfo.map(item => {
                if (item.isSelected) {
                    activeItemId = item.optionId;
                }
            })
        }
        return activeItemId;
    }

    toggleView() {
        this.isDesktopView = !this.isDesktopView;
    }

    backToEdit() {
        this.router.navigate(['../site-sections'], {relativeTo: this.route});
    }

    ngOnDestroy() {
        //
    }
}
