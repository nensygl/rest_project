import {ChangeDetectionStrategy, Component, OnInit} from '@angular/core';
import {Store} from '@ngrx/store';
import {AppState} from '../../store';
import {FormBuilder, FormGroup} from '@angular/forms';
import {MINUTE} from '../../utils/constants';
import {LoginSelectors} from '../../store/login';
import * as LoginActions from '../../store/login/login.actions';
import {TuiCountryIsoCode} from '@taiga-ui/i18n';

@Component({
    selector: 'app-landing-header',
    templateUrl: './landing-header.component.html',
    styleUrls: ['./landing-header.component.less'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class LandingHeaderComponent implements OnInit {
    displayMenu: boolean = false;
    isDisplayLoginModal: boolean = false;
    phoneForm: FormGroup;
    isPhoneNumValid: boolean = true;
    isDisplayCodeInput: boolean = false;
    isDisplayCodeTimer: boolean = true;
    codeTimerCounter: number = MINUTE;
    timer: any;
    readonly countries: Array<TuiCountryIsoCode> = Object.values(TuiCountryIsoCode);
    countryIsoCode = TuiCountryIsoCode.US;


    constructor(
        private readonly store: Store<AppState>,
        private fb: FormBuilder
    ) {
        this.store.select(LoginSelectors.getIsDisplayCodeInput)
            .subscribe((isDisplay) => {
                console.log('isDisplay', isDisplay);
                this.isDisplayCodeInput = isDisplay;
                if (isDisplay) {
                    this.startTimer();
                }
            });
        this.phoneForm = this.fb.group({
            phone: ['']
        });
    }

    ngOnInit(): void {
        // this.isDisplayLoginModal = this.store.select(LoginSelectors.getIsDisplayLoginModal);
    }

    toggleLoginModal() {
        // this.store.dispatch(LoginActions.displayLoginModalChange({isDisplayLoginModal: true}));
        this.isDisplayLoginModal = !this.isDisplayLoginModal;
    }

    toggleMobMenu() {
        this.displayMenu = !this.displayMenu;
    }

    onSendPhoneNum() {
        const formVal = this.phoneForm.getRawValue();
        const phoneNum = formVal.phone;
        console.log(phoneNum);
        if (this.isPhoneNumValid) {
            console.log('phone valid');
            this.store.dispatch(LoginActions.getSMSCode({phoneNum}));
        }
    }

    getNewCode() {
        this.onSendPhoneNum();
    }

    onCodeChanged(code: string) {
        console.log(code);
    }

    onCodeCompleted(code: string) {
        console.log('completed code', code);
        this.store.dispatch(LoginActions.checkSMSCode({code}));
    }

    startTimer() {
        this.isDisplayCodeTimer = true;
        this.timer = setInterval(() => {
            if (this.codeTimerCounter <= MINUTE && this.codeTimerCounter > 0) {
                this.codeTimerCounter--;
            } else {
                this.stopTimer();
            }
        }, 1000);
    }

    stopTimer() {
        clearInterval(this.timer);
        this.isDisplayCodeTimer = false;
    }
}
