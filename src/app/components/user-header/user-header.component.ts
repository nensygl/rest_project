import {Component, OnInit} from '@angular/core';

@Component({
    selector: 'app-user-header',
    templateUrl: './user-header.component.html',
    styleUrls: ['./user-header.component.less']
})
export class UserHeaderComponent implements OnInit {
    displayLoginHint: boolean = false;

    constructor() {
    }

    ngOnInit(): void {
    }

    toggleDisplayLoginHint() {
        this.displayLoginHint = !this.displayLoginHint;
    }

}
