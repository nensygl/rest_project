import {Component, OnInit} from '@angular/core';
import {SelectedSectionModel} from '../../models/site-settings.model';
import {Store} from '@ngrx/store';
import {AppState} from '../../store';
import {SiteSettingsSelectors} from '../../store/site-settings';
import * as SiteSettingsActions from '../../store/site-settings';
import {displaySectionsInfo} from '../../utils/constants';
import {SectionsEnum} from '../../enums/sections.enum';
import * as _ from 'lodash';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
    selector: 'app-site-sections-settings',
    templateUrl: './site-sections-settings.component.html',
    styleUrls: ['./site-sections-settings.component.less']
})
export class SiteSectionsSettingsComponent implements OnInit {
    siteSectionsEnum = SectionsEnum;
    selectedSections: SelectedSectionModel[];
    displaySectionsInfo = displaySectionsInfo;

    constructor(
        private readonly store: Store<AppState>,
        private readonly router: Router,
        private readonly route: ActivatedRoute
    ) {
        this.store.select(SiteSettingsSelectors.getSelectedSections)
            .subscribe(sections => this.selectedSections = sections);
    }

    ngOnInit(): void {
        this.store.dispatch(SiteSettingsActions.getSelectedSections.request());
    }

    isSectionActive(sectionId: number, itemId: number) {
        const index = this.displaySectionsInfo.findIndex(item => item.sectionId === sectionId);
        if (index !== -1) {
            return this.displaySectionsInfo[index].displayingItemNo === itemId;
        }
        return false;
    }

    onSaveClick() {
        console.log('save clicked');
    }

    openPreview() {
        this.router.navigate(['../preview'], {relativeTo: this.route});
    }

    displayNextSection(sectionId: number) {
        const index = this.displaySectionsInfo.findIndex(item => item.sectionId === sectionId);
        if (index !== -1) {
            const activeSection = this.displaySectionsInfo[index];
            if (activeSection.displayingItemNo === activeSection.sectionItemsLen) {
                activeSection.displayingItemNo = 0;
            }
            activeSection.displayingItemNo++;
        }
    }

    displayPrevSection(sectionId: number) {
        const index = this.displaySectionsInfo.findIndex(item => item.sectionId === sectionId);
        if (index !== -1) {
            const activeSection = this.displaySectionsInfo[index];
            activeSection.displayingItemNo--;
            if (activeSection.displayingItemNo === 0) {
                activeSection.displayingItemNo = activeSection.sectionItemsLen;
            }
        }
    }

    isSectionItemSelected(sectionId: number, itemId: number) {
        const indexSection = this.selectedSections.findIndex(item => item.sectionId === sectionId);
        if (indexSection !== -1) {
            const selected = this.selectedSections[indexSection];
            const indexItem = selected.selectionInfo.findIndex(item => item.optionId === itemId);
            if (indexItem !== -1) {
                return selected.selectionInfo[indexItem].isSelected;
            }
        }
        return false;
    }

    toggleSectionItemSelection(sectionId: number, itemId: number) {
        const indexSection = this.selectedSections.findIndex(item => item.sectionId === sectionId);
        if (indexSection !== -1) {
            const selectedSections = _.cloneDeep(this.selectedSections);
            const selected = selectedSections[indexSection];
            selected.selectionInfo.map(item => {
                if (item.optionId !== itemId) {
                    item.isSelected = false;
                } else {
                    item.isSelected = !item.isSelected;
                }
            });
            this.store.dispatch(SiteSettingsActions.updateSelectedSections.request({selectedSections}));
        }
    }
}
