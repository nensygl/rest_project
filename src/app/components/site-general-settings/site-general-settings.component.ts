import {Component, OnInit} from '@angular/core';
import {SettingsItemsEnum} from '../../enums/settings-items.enum';

@Component({
    selector: 'app-site-general-settings',
    templateUrl: './site-general-settings.component.html',
    styleUrls: ['./site-general-settings.component.less']
})
export class SiteGeneralSettingsComponent implements OnInit {
    settingsItemsEnum = SettingsItemsEnum;
    activeSettingsItem: number = 7;

    constructor() {
    }

    ngOnInit(): void {
        //
    }

    isSettingsItemActive(settingsItem: number) {
        return this.activeSettingsItem === settingsItem;
    }

    changeActiveItem(settingsItem: number) {
        this.activeSettingsItem = settingsItem;
    }
}
