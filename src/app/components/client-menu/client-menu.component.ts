import {Component, OnDestroy, OnInit} from '@angular/core';
import {Store} from '@ngrx/store';
import {AppState} from '../../store';
import {ActivatedRoute, Router} from '@angular/router';
import {UntilDestroy, untilDestroyed} from '@ngneat/until-destroy';
import {MenuItemModel, MenuModel} from '../../models/menu.model';
import * as ClientActions from '../../store/client';
import {ClientSelectors} from '../../store/client';
import {Observable} from 'rxjs';

@UntilDestroy()
@Component({
    selector: 'app-client-menu',
    templateUrl: './client-menu.component.html',
    styleUrls: ['./client-menu.component.less']
})
export class ClientMenuComponent implements OnInit, OnDestroy {

    menu: Observable<MenuModel[]>;
    cart: MenuItemModel[];

    constructor(
        private readonly store: Store<AppState>,
        private readonly router: Router,
        private readonly route: ActivatedRoute
    ) {
        this.menu = this.store.select(ClientSelectors.getMenu);
        this.store.select(ClientSelectors.getCart)
            .pipe(untilDestroyed(this))
            .subscribe(cart => {
                this.cart = cart;
            });
    }

    ngOnInit(): void {
        this.store.dispatch(ClientActions.getMenu.request());
    }

    ngOnDestroy() {
        //
    }

    addToCard(item: MenuItemModel) {
        this.store.dispatch(ClientActions.addItemToCart({ item }));
    }

    removeFromCart(item: MenuItemModel) {
        this.store.dispatch(ClientActions.removeItemFromCart({ item }));
    }

    isItemInCart(id: number) {
        return this.cart.findIndex(cartItem => cartItem.id === id) >= 0;
    }

    getItemQuantity(id: number) {
        const index = this.cart.findIndex(cartItem => cartItem.id === id);
        return this.cart[index].quantity;
    }
}
