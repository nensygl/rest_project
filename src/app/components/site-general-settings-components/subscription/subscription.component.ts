import {Component, OnDestroy, OnInit} from '@angular/core';
import {Store} from '@ngrx/store';
import {AppState} from '../../../store';
import {SiteSettingsSelectors} from '../../../store/site-settings';
import * as SiteSettingsActions from '../../../store/site-settings';
import {UntilDestroy, untilDestroyed} from '@ngneat/until-destroy';
import {TariffPlanModel, UserSubscriptionDataModel} from '../../../models/site-general-settings.model';
import {tariffPlans} from '../../../dummy-data/site-settings-info';

@UntilDestroy()
@Component({
    selector: 'app-subscription',
    templateUrl: './subscription.component.html',
    styleUrls: ['./subscription.component.less']
})
export class SubscriptionComponent implements OnInit, OnDestroy {
    subscriptionData: UserSubscriptionDataModel;
    tariffPlans: TariffPlanModel[] = tariffPlans;

    constructor(
        private readonly store: Store<AppState>
    ) {
        this.store.select(SiteSettingsSelectors.getSubscriptionData)
            .pipe(untilDestroyed(this))
            .subscribe(data => {
                if (data) {
                    this.subscriptionData = data;
                } else {
                    this.store.dispatch(SiteSettingsActions.getSubscriptionData.request());
                }
            });
    }

    ngOnInit(): void {
        //
    }

    getDate(date: Date) {
        let day = date.getDate().toString();
        if (day.length < 2) {
            day = '0' + day;
        }
        let month = (date.getMonth() + 1).toString();
        if (month.length < 2) {
            month = '0' + month;
        }
        let year = date.getFullYear();
        return `${day}.${month}.${year}`;
    }

    cancelSubscription() {
        this.store.dispatch(SiteSettingsActions.cancelSubscription.request());
    }

    selectTariff() {
        console.log('tariff was clicked');
    }

    ngOnDestroy() {
        //
    }
}
