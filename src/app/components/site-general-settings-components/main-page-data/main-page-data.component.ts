import {Component, OnDestroy, OnInit} from '@angular/core';
import {UntilDestroy, untilDestroyed} from '@ngneat/until-destroy';
import {Store} from '@ngrx/store';
import {AppState} from '../../../store';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {SiteSettingsSelectors} from '../../../store/site-settings';
import * as SiteSettingsActions from '../../../store/site-settings';
import {SiteSettingsDataModel} from '../../../models/site-general-settings.model';
import {FormFiledTypesEnum} from '../../../enums/form-filed-types.enum';
import {TUI_VALIDATION_ERRORS} from '@taiga-ui/kit';
import {maxLengthValidator, minLengthValidator} from '../../../utils/functions';

@UntilDestroy()
@Component({
    selector: 'app-main-page-data',
    templateUrl: './main-page-data.component.html',
    styleUrls: ['./main-page-data.component.less'],
    providers: [
        {
            provide: TUI_VALIDATION_ERRORS,
            useValue: {
                required: 'Поле обязаельно для заполнения',
                maxlength: maxLengthValidator,
                minlength: minLengthValidator,
            }
        }
    ]
})
export class MainPageDataComponent implements OnInit, OnDestroy {
    mainPageData: SiteSettingsDataModel[];
    mainPageDataForm: FormGroup;
    formFieldType = FormFiledTypesEnum;

    constructor(
        private readonly store: Store<AppState>,
        private readonly fb: FormBuilder
    ) {
        this.mainPageDataForm = this.fb.group({});
        this.store.select(SiteSettingsSelectors.getMainPageData)
            .pipe(untilDestroyed(this))
            .subscribe(data => {
                if (data) {
                    this.mainPageData = data;
                    this.fillForm(data);
                } else {
                    this.store.dispatch(SiteSettingsActions.getMainPageData.request());
                }
            });
    }

    ngOnInit(): void {
        //
    }

    fillForm(data: SiteSettingsDataModel[]) {
        data.forEach(section => {
            const sectionGroup = this.fb.group({});
            section.sectionDataFields.forEach(filed => {
                const validators = [];
                for (const [key, value] of Object.entries(filed.validators)) {
                    switch (key) {
                        case 'min':
                            validators.push(Validators.min(value));
                            break;
                        case 'max':
                            validators.push(Validators.max(value));
                            break;
                        case 'required':
                            if (value) {
                                validators.push(Validators.required);
                            }
                            break;
                        case 'requiredTrue':
                            if (value) {
                                validators.push(Validators.requiredTrue);
                            }
                            break;
                        case 'email':
                            if (value) {
                                validators.push(Validators.email);
                            }
                            break;
                        case 'minLength':
                            validators.push(Validators.minLength(value));
                            break;
                        case 'maxLength':
                            validators.push(Validators.maxLength(value));
                            break;
                        case 'pattern':
                            validators.push(Validators.pattern(value));
                            break;
                        case 'nullValidator':
                            if (value) {
                                validators.push(Validators.nullValidator);
                            }
                            break;
                        default:
                            break;
                    }
                }
                sectionGroup.addControl(
                    filed.fieldKey,
                    this.fb.control(filed.fieldValue, validators)
                );
            });
            this.mainPageDataForm.addControl(section.sectionKey, sectionGroup);
        });
        console.log('mainPageDataForm', this.mainPageDataForm.value);
    }

    ngOnDestroy() {
        //
    }

    resetForm() {
        this.fillForm(this.mainPageData);
    }

    submitForm() {
        const mainPageData = this.mainPageDataForm.value;
        this.store.dispatch(SiteSettingsActions.updateMainPageData.request({ mainPageData }));
    }
}
