import {Component, OnDestroy, OnInit} from '@angular/core';
import {UntilDestroy, untilDestroyed} from '@ngneat/until-destroy';
import {Store} from '@ngrx/store';
import {AppState} from '../../../store';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {SiteSettingsSelectors} from '../../../store/site-settings';
import * as SiteSettingsActions from '../../../store/site-settings';
import {SiteDomainSettingsModel} from '../../../models/site-general-settings.model';

@UntilDestroy()
@Component({
    selector: 'app-domain',
    templateUrl: './domain.component.html',
    styleUrls: ['./domain.component.less']
})
export class DomainComponent implements OnInit, OnDestroy {
    siteDomainData: SiteDomainSettingsModel;
    siteDomainForm: FormGroup;
    isDisplayDomainAvailability: boolean = false;
    isDomainAvailable: boolean;

    constructor(
        private readonly store: Store<AppState>,
        private readonly formBuilder: FormBuilder
    ) {
        this.siteDomainForm = this.formBuilder.group({
            domainName: new FormControl('', Validators.required)
        });
        this.store.select(SiteSettingsSelectors.getSiteDomainData)
            .pipe(untilDestroyed(this))
            .subscribe(data => {
                if (data) {
                    this.siteDomainData = data;
                    this.fillForm(data);
                } else {
                    this.store.dispatch(SiteSettingsActions.getSiteDomainData.request());
                }
            });
        this.store.select(SiteSettingsSelectors.getIsDomainAvailable)
            .pipe(untilDestroyed(this))
            .subscribe(data => {
                if (typeof data !== "undefined") {
                    this.isDomainAvailable = data;
                    this.isDisplayDomainAvailability = true;
                }
            });
    }

    ngOnInit(): void {
        //
    }

    fillForm(data: SiteDomainSettingsModel) {
        this.siteDomainForm.patchValue({
            domainName: data.domainName
        });
    }

    resetForm() {
        this.fillForm(this.siteDomainData);
    }

    submitForm() {
        const siteDomainData = this.siteDomainForm.getRawValue();
        this.store.dispatch(SiteSettingsActions.updateSiteDomainData.request({siteDomainData}));
        this.isDisplayDomainAvailability = false;
    }

    checkIsDomainAvailable() {
        this.isDisplayDomainAvailability = false;
        const domainForm = this.siteDomainForm.getRawValue();
        this.store.dispatch(SiteSettingsActions.checkIsDomainAvailable.request({domainName: domainForm.domainName}));
    }

    ngOnDestroy() {
        //
    }

}
