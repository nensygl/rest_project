import {Component, OnDestroy, OnInit} from '@angular/core';
import {Store} from '@ngrx/store';
import {AppState} from '../../../store';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {UntilDestroy, untilDestroyed} from '@ngneat/until-destroy';
import {PaymentSystemSettingsModel} from '../../../models/site-general-settings.model';
import {SiteSettingsSelectors} from '../../../store/site-settings';
import * as SiteSettingsActions from '../../../store/site-settings';

@UntilDestroy()
@Component({
    selector: 'app-payment-system',
    templateUrl: './payment-system.component.html',
    styleUrls: ['./payment-system.component.less']
})
export class PaymentSystemComponent implements OnInit, OnDestroy {
    paymentSystemData: PaymentSystemSettingsModel;
    paymentSystemForm: FormGroup;

    constructor(
        private readonly store: Store<AppState>,
        private readonly formBuilder: FormBuilder
    ) {
        this.paymentSystemForm = this.formBuilder.group({
            publishableKey: new FormControl('', Validators.required),
            secretKey: new FormControl('', Validators.required),
            webhookUrl: new FormControl(''),
            signingSecret: new FormControl('', Validators.required)
        });
        this.store.select(SiteSettingsSelectors.getPaymentSystemData)
            .pipe(untilDestroyed(this))
            .subscribe(data => {
                if (data) {
                    this.paymentSystemData = data;
                    this.fillForm(data);
                } else {
                    this.store.dispatch(SiteSettingsActions.getPaymentSystemData.request());
                }
            });
    }

    ngOnInit(): void {
        //
    }

    fillForm(data: PaymentSystemSettingsModel) {
        this.paymentSystemForm.patchValue({
            publishableKey: data.publishableKey,
            secretKey: data.secretKey,
            webhookUrl: data.webhookUrl,
            signingSecret: data.signingSecret
        });
    }

    resetForm() {
        this.fillForm(this.paymentSystemData);
    }

    submitForm() {
        if (this.paymentSystemForm.valid) {
            const paymentSystemData = this.paymentSystemForm.getRawValue();
            this.store.dispatch(SiteSettingsActions.updatePaymentSystemData.request({paymentSystemData}));
        }
    }

    ngOnDestroy() {
        //
    }

}
