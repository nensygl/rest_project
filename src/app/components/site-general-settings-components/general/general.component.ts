import {Component, OnDestroy, OnInit} from '@angular/core';
import {Store} from '@ngrx/store';
import {AppState} from '../../../store';
import * as SiteSettingsActions from '../../../store/site-settings';
import {SiteSettingsSelectors} from '../../../store/site-settings';
import {UntilDestroy, untilDestroyed} from '@ngneat/until-destroy';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {SiteGeneralSettingsModel} from '../../../models/site-general-settings.model';
import {TUI_VALIDATION_ERRORS} from '@taiga-ui/kit';
import {maxLengthValidator, minLengthValidator} from '../../../utils/functions';

@UntilDestroy()
@Component({
    selector: 'app-general',
    templateUrl: './general.component.html',
    styleUrls: ['./general.component.less'],
    providers: [
        {
            provide: TUI_VALIDATION_ERRORS,
            useValue: {
                required: 'Поле обязаельно для заполнения',
                maxlength: maxLengthValidator,
                minlength: minLengthValidator,
            }
        }
    ]
})
export class GeneralComponent implements OnInit, OnDestroy {
    generalData: SiteGeneralSettingsModel;
    generalDataForm: FormGroup;

    constructor(
        private readonly store: Store<AppState>,
        private readonly formBuilder: FormBuilder
    ) {
        this.generalDataForm = this.formBuilder.group({
            siteName: new FormControl('', [
                Validators.required,
                Validators.minLength(5),
                Validators.maxLength(100)
            ]),
            siteDescription: new FormControl('', [
                Validators.required,
                Validators.maxLength(250)
            ]),
            placeFormat: new FormControl('', [
                Validators.required,
                Validators.maxLength(100)
            ]),
        });
        this.store.select(SiteSettingsSelectors.getGeneralData)
            .pipe(untilDestroyed(this))
            .subscribe(generalData => {
                if (generalData) {
                    this.generalData = generalData;
                    this.fillForm(generalData);
                } else {
                    this.store.dispatch(SiteSettingsActions.getGeneralSettingsData.request());
                }
            });
    }

    ngOnInit(): void {
        //
    }

    fillForm(data: SiteGeneralSettingsModel) {
        this.generalDataForm.patchValue({
            siteName: data.siteName,
            siteDescription: data.siteDescription,
            placeFormat: data.placeFormat
        });
    }

    resetForm() {
        this.fillForm(this.generalData);
    }

    submitForm() {
        if (this.generalDataForm.valid) {
            const generalData: SiteGeneralSettingsModel = this.generalDataForm.getRawValue();
            console.log(generalData);
            this.store.dispatch(SiteSettingsActions.updateGeneralSettingsData.request({generalData}));
        }
    }

    ngOnDestroy() {
        //
    }

}
