import {ChangeDetectionStrategy, Component, OnDestroy, OnInit} from '@angular/core';
import {UntilDestroy, untilDestroyed} from '@ngneat/until-destroy';
import {Store} from '@ngrx/store';
import {AppState} from '../../../store';
import {FontListItemModel, FontWeightItemModel, SiteColorsAndFontsSettingsModel} from '../../../models/site-general-settings.model';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {SiteSettingsSelectors} from '../../../store/site-settings';
import * as SiteSettingsActions from '../../../store/site-settings';
import {TUI_VALIDATION_ERRORS} from '@taiga-ui/kit';
import {maxLengthValidator, minLengthValidator} from '../../../utils/functions';
import {TuiContextWithImplicit, tuiPure, TuiStringHandler} from '@taiga-ui/cdk';
import {defaultEditorColors} from '@taiga-ui/addon-editor';

@UntilDestroy()
@Component({
    selector: 'app-color-and-fonts',
    templateUrl: './color-and-fonts.component.html',
    styleUrls: ['./color-and-fonts.component.less'],
    changeDetection: ChangeDetectionStrategy.OnPush,
    providers: [
        {
            provide: TUI_VALIDATION_ERRORS,
            useValue: {
                required: 'Поле обязаельно для заполнения',
                maxlength: maxLengthValidator,
                minlength: minLengthValidator,
            }
        }
    ]
})
export class ColorAndFontsComponent implements OnInit, OnDestroy {
    readonly palette = defaultEditorColors;
    colorsAndFontsData: SiteColorsAndFontsSettingsModel;
    headerForm: FormGroup;
    textForm: FormGroup;
    buttonsForm: FormGroup;
    colorsForm: FormGroup;
    fontsList: FontListItemModel[];
    fontWeightList: FontWeightItemModel[];

    headerColor: string = '';
    textColor: string = '';
    btnColor: string = '';
    backgroundColor: string = '';

    constructor(
        private readonly store: Store<AppState>,
        private readonly formBuilder: FormBuilder
    ) {
        this.headerForm = this.formBuilder.group({
            fontFamily: new FormControl('', Validators.required),
            fontColor: new FormControl('', Validators.required),
            fontWeight: new FormControl('', Validators.required),
            fontSize: new FormControl('', Validators.required)
        });
        this.textForm = this.formBuilder.group({
            fontFamily: new FormControl('', Validators.required),
            fontColor: new FormControl('', Validators.required),
            fontWeight: new FormControl('', Validators.required),
            fontSize: new FormControl('', Validators.required)
        });
        this.buttonsForm = this.formBuilder.group({
            fontFamily: new FormControl('', Validators.required),
            fontColor: new FormControl('', Validators.required),
            fontWeight: new FormControl('', Validators.required),
            fontSize: new FormControl('', Validators.required)
        });

        this.colorsForm = this.formBuilder.group({
            backgroundColor: new FormControl('', Validators.required)
        });
        this.store.select(SiteSettingsSelectors.getColorsAndFontsData)
            .pipe(untilDestroyed(this))
            .subscribe(data => {
                if (data) {
                    this.colorsAndFontsData = data;
                    this.headerColor = data.header.fontColor;
                    this.textColor = data.text.fontColor;
                    this.btnColor = data.buttons.fontColor;
                    this.backgroundColor = data.backgroundColor;
                    this.fillForm(data);
                } else {
                    this.store.dispatch(SiteSettingsActions.getColorsAndFontsSettingsData.request());
                }
            });
        this.store.select(SiteSettingsSelectors.getFontsList)
            .pipe(untilDestroyed(this))
            .subscribe(fontList => {
                if (fontList) {
                    this.fontsList = fontList;
                } else {
                    this.store.dispatch(SiteSettingsActions.getFontsList.request());
                }
            });
        this.store.select(SiteSettingsSelectors.getFontWeightList)
            .pipe(untilDestroyed(this))
            .subscribe(fontWeightList => {
                if (fontWeightList) {
                    this.fontWeightList = fontWeightList;
                } else {
                    this.store.dispatch(SiteSettingsActions.getFontWeightList.request());
                }
            });
    }

    ngOnInit(): void {
        //
    }

    @tuiPure
    stringifyFont(
        fontsList: readonly FontListItemModel[],
    ): TuiStringHandler<TuiContextWithImplicit<number>> {
        const map = new Map(fontsList.map(({fontId, fontName}) => [fontId, fontName] as [number, string]));

        return ({$implicit}: TuiContextWithImplicit<number>) => map.get($implicit) || '';
    }

    @tuiPure
    stringifyFontWeight(
        fontWeightList: readonly FontWeightItemModel[],
    ): TuiStringHandler<TuiContextWithImplicit<number>> {
        const map = new Map(fontWeightList.map(({fontWeight, fontWeightName}) => [fontWeight, fontWeightName] as [number, string]));

        return ({$implicit}: TuiContextWithImplicit<number>) => map.get($implicit) || '';
    }

    fillForm(data: SiteColorsAndFontsSettingsModel) {
        this.headerForm.patchValue({
            fontFamily: data.header.fontFamily,
            fontColor: data.header.fontColor,
            fontWeight: data.header.fontWeight,
            fontSize: data.header.fontSize
        });
        this.textForm.patchValue({
            fontFamily: data.text.fontFamily,
            fontColor: data.text.fontColor,
            fontWeight: data.text.fontWeight,
            fontSize: data.text.fontSize
        });
        this.buttonsForm.patchValue({
            fontFamily: data.buttons.fontFamily,
            fontColor: data.buttons.fontColor,
            fontWeight: data.buttons.fontWeight,
            fontSize: data.buttons.fontSize
        });
        this.colorsForm.patchValue({
            backgroundColor: data.backgroundColor
        });
    }

    resetForm() {
        this.fillForm(this.colorsAndFontsData);
    }

    submitForm() {
        if (this.headerForm.valid && this.textForm.valid && this.buttonsForm.valid && this.colorsForm.valid) {
            const header = this.headerForm.getRawValue();
            const text = this.textForm.getRawValue();
            const buttons = this.buttonsForm.getRawValue();
            const colors = this.colorsForm.getRawValue();
            const colorsAndFonts: SiteColorsAndFontsSettingsModel = {
                header,
                text,
                buttons,
                backgroundColor: colors.backgroundColor
            };
            console.log(colorsAndFonts);
            this.store.dispatch(SiteSettingsActions.updateColorsAndFontsSettingsData.request({colorsAndFonts}));
        }
    }

    onChangeColor(color: string, item: string) {
        switch (item) {
            case 'header':
                this.headerColor = color;
                this.headerForm.patchValue({
                    fontColor: color
                });
                break;
            case 'text':
                this.textColor = color;
                this.textForm.patchValue({
                    fontColor: color
                });
                break;
            case 'btn':
                this.btnColor = color;
                this.buttonsForm.patchValue({
                    fontColor: color
                });
                break;
            case 'background':
                this.backgroundColor = color;
                this.colorsForm.patchValue({
                    fontColor: color
                });
                break;
        }
    }

    ngOnDestroy() {
        //
    }
}
