import {Component, OnDestroy, OnInit} from '@angular/core';
import {UntilDestroy, untilDestroyed} from '@ngneat/until-destroy';
import {Store} from '@ngrx/store';
import {AppState} from '../../../store';
import {SiteSettingsSelectors} from '../../../store/site-settings';
import * as SiteSettingsActions from '../../../store/site-settings';

@UntilDestroy()
@Component({
    selector: 'app-menu-cards-format',
    templateUrl: './menu-cards-format.component.html',
    styleUrls: ['./menu-cards-format.component.less']
})
export class MenuCardsFormatComponent implements OnInit, OnDestroy {
    selectedMenuCardsStyle: number;

    constructor(
        private readonly store: Store<AppState>
    ) {
        this.store.select(SiteSettingsSelectors.getSelectedMenuCardsStyle)
            .pipe(untilDestroyed(this))
            .subscribe(value => {
                if (value) {
                    this.selectedMenuCardsStyle = value;
                } else {
                    this.store.dispatch(SiteSettingsActions.getSelectedMenuCardsStyle.request());
                }
            });
    }

    ngOnInit(): void {
        //
    }

    updateSelectedMenuCardsStyle(style: number) {
        this.store.dispatch(SiteSettingsActions.updateSelectedMenuCardsStyle.request({selectedMenuCardsStyle: style}));
    }

    ngOnDestroy() {
        //
    }

}
