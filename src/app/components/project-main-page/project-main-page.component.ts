import {Component, OnInit} from '@angular/core';
import {Store} from '@ngrx/store';
import {AppState} from '../../store';
import {Observable} from 'rxjs';
import * as ProjectActions from '../../store/project/project.actions';
import {ProjectSelectors} from '../../store/project';
import {ProjectBaseInfoModel} from '../../models/project-base-info.model';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
    selector: 'app-project-main-page',
    templateUrl: './project-main-page.component.html',
    styleUrls: ['./project-main-page.component.less']
})
export class ProjectMainPageComponent implements OnInit {

    isProjectCreated$: Observable<boolean>;
    projectBaseInfo$: Observable<ProjectBaseInfoModel | undefined>;

    constructor(
        private readonly store: Store<AppState>,
        private readonly router: Router,
        private readonly route: ActivatedRoute
    ) {
        this.isProjectCreated$ = this.store.select(ProjectSelectors.getIsProjectCreated);
        this.projectBaseInfo$ = this.store.select(ProjectSelectors.getProjectBaseInfo);
    }

    ngOnInit(): void {
        this.store.dispatch(ProjectActions.checkIsProjectCreated.request());
    }

    createProject() {
        this.store.dispatch(ProjectActions.createProject.request());
        console.log(this.isProjectCreated$);
    }

    editMenu() {
        this.router.navigate(['../menu'], {relativeTo: this.route});
    }

    editStopList() {
        this.router.navigate(['../stop-list'], {relativeTo: this.route});
    }

    navToSectionsSettings() {
        this.router.navigate(['../site-sections'], {relativeTo: this.route});
    }

    navToGeneralSettings() {
        this.router.navigate(['../general-settings'], {relativeTo: this.route});
    }
}
