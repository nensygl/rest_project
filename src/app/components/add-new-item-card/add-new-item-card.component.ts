import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
    selector: 'app-add-new-item-card',
    templateUrl: './add-new-item-card.component.html',
    styleUrls: ['./add-new-item-card.component.less']
})
export class AddNewItemCardComponent implements OnInit {

    constructor(
        private readonly router: Router,
        private readonly route: ActivatedRoute
    ) {
    }

    ngOnInit(): void {
    }

    addNewItem() {
        console.log('add new item clicked');
        this.router.navigate(['new'], {relativeTo: this.route});
    }
}
