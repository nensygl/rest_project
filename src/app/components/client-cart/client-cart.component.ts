import {Component, OnDestroy, OnInit} from '@angular/core';
import {Store} from '@ngrx/store';
import {AppState} from '../../store';
import {ActivatedRoute, Router} from '@angular/router';
import {MenuItemModel} from '../../models/menu.model';
import {ClientSelectors} from '../../store/client';
import * as ClientActions from '../../store/client';
import {UntilDestroy, untilDestroyed} from '@ngneat/until-destroy';

@UntilDestroy()
@Component({
    selector: 'app-client-cart',
    templateUrl: './client-cart.component.html',
    styleUrls: ['./client-cart.component.less']
})
export class ClientCartComponent implements OnInit, OnDestroy {
    displayCart: boolean;
    cart: MenuItemModel[];

    constructor(
        private readonly store: Store<AppState>,
        private readonly router: Router,
        private readonly route: ActivatedRoute
    ) {
        this.store.select(ClientSelectors.getCart)
            .pipe(untilDestroyed(this))
            .subscribe(cart => {
                this.cart = cart;
            });
        this.store.select(ClientSelectors.getDisplayCart)
            .pipe(untilDestroyed(this))
            .subscribe(displayCart => {
                this.displayCart = displayCart;
                displayCart ? document.body.style.overflow = 'hidden' : document.body.style.overflow = 'auto';
            });
    }

    ngOnInit(): void {
        //
    }

    ngOnDestroy() {
        //
    }

    closeCart() {
        this.store.dispatch(ClientActions.toggleCartDisplay({ displayCart: false }));
    }

    removeFromCart(item: MenuItemModel) {
        this.store.dispatch(ClientActions.removeItemFromCart({item}));
    }

    addToCard(item: MenuItemModel) {
        this.store.dispatch(ClientActions.addItemToCart({item}));
    }

    getCartTotalInfo(cart: MenuItemModel[]) {
        const itemLen: number = this.getCartLen(cart);
        let itemWordForm: string = this.getItemWordForm(itemLen);
        let sum: number = this.getItemsSum(cart);
        return `${itemLen} ${itemWordForm} на ${sum}`;
    }

    getCartLen(cart: MenuItemModel[]) {
        let counter = 0;
        cart.forEach(item => counter += item.quantity || 0);
        return counter;
    }

    getItemWordForm(cartLen: number) {
        let wordForm = '';
        switch (true) {
            case (cartLen === 1):
                wordForm = 'товар';
                break;
            case (cartLen >= 2 && cartLen <= 4):
                wordForm = 'товара';
                break;
            case (cartLen >= 5 && cartLen <= 20):
                wordForm = 'товаров';
                break;
            case (cartLen % 10 === 0):
                wordForm = 'товаров';
                break;
            case (cartLen % 10 === 1):
                wordForm = 'товар';
                break;
            case (cartLen % 10 >= 2 && cartLen % 10 <= 4):
                wordForm = 'товара';
                break;
            case (cartLen % 10 >= 5 && cartLen % 10 <= 9):
                wordForm = 'товаров';
                break;
            default:
                wordForm = 'товар';
        }
        return wordForm;
    }

    getItemsSum(cart: MenuItemModel[]) {
        let sum = 0;
        cart.forEach(item => sum += (item.itemPrice * (item.quantity || 0)));
        return sum;
    }

    goToCart() {
        console.log('go to cart clicked');
    }
}
