import {ProjectBaseInfoModel} from '../models/project-base-info.model';

export const projectData: ProjectBaseInfoModel = {
  projectName: 'Sumo Bistro',
  projectURL: 'https://sumo.com'
}
