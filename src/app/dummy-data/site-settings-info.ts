import {SelectedSectionModel} from '../models/site-settings.model';
import {SectionsEnum} from '../enums/sections.enum';
import {
    FontListItemModel, FontWeightItemModel,
    PaymentSystemSettingsModel,
    SiteColorsAndFontsSettingsModel, SiteDomainSettingsModel,
    SiteGeneralSettingsModel, SiteSettingsDataModel, TariffPlanModel, UserSubscriptionDataModel
} from '../models/site-general-settings.model';
import {FormFiledTypesEnum} from '../enums/form-filed-types.enum';

export const selectedSections: SelectedSectionModel[] = [
    {
        sectionId: SectionsEnum.cover,
        selectionInfo: [
            {
                optionId: 1,
                isSelected: true
            },
            {
                optionId: 2,
                isSelected: false
            }
        ]
    }, {
        sectionId: SectionsEnum.buttons,
        selectionInfo: [
            {
                optionId: 1,
                isSelected: true
            },
            {
                optionId: 2,
                isSelected: false
            }
        ]
    }, {
        sectionId: SectionsEnum.menu,
        selectionInfo: [
            {
                optionId: 1,
                isSelected: true
            },
            {
                optionId: 2,
                isSelected: false
            }
        ]
    }, {
        sectionId: SectionsEnum.text,
        selectionInfo: [
            {
                optionId: 1,
                isSelected: true
            },
            {
                optionId: 2,
                isSelected: false
            },
            {
                optionId: 3,
                isSelected: false
            },
            {
                optionId: 4,
                isSelected: false
            }
        ]
    }, {
        sectionId: SectionsEnum.banner,
        selectionInfo: [
            {
                optionId: 1,
                isSelected: true
            },
            {
                optionId: 2,
                isSelected: false
            }
        ]
    }, {
        sectionId: SectionsEnum.gallery,
        selectionInfo: [
            {
                optionId: 1,
                isSelected: true
            },
            {
                optionId: 2,
                isSelected: false
            }
        ]
    }, {
        sectionId: SectionsEnum.map,
        selectionInfo: [
            {
                optionId: 1,
                isSelected: true
            },
            {
                optionId: 2,
                isSelected: false
            }
        ]
    }, {
        sectionId: SectionsEnum.contacts,
        selectionInfo: [
            {
                optionId: 1,
                isSelected: true
            },
            {
                optionId: 2,
                isSelected: false
            }
        ]
    }
];

export const generalSettingsData: SiteGeneralSettingsModel = {
    favicon: null,
    siteName: '',
    siteDescription: '',
    placeFormat: ''
};

export const colorsAndFontsData: SiteColorsAndFontsSettingsModel = {
    header: {
        fontFamily: 1,
        fontColor: '#000000',
        fontWeight: 600,
        fontSize: 24
    },
    text: {
        fontFamily: 1,
        fontColor: '#000000',
        fontWeight: 400,
        fontSize: 16
    },
    buttons: {
        fontFamily: 1,
        fontColor: '#000000',
        fontWeight: 400,
        fontSize: 16
    },
    backgroundColor: '#FFFFFF'
};

export const fontList: FontListItemModel[] = [
    {
        fontId: 1,
        fontName: 'Roboto'
    },
    {
        fontId: 2,
        fontName: 'Helvetica'
    },
    {
        fontId: 3,
        fontName: 'Golos VF'
    }
];

export const fontWeightList: FontWeightItemModel[] = [
    {
        fontWeight: 100,
        fontWeightName: '100'
    },
    {
        fontWeight: 200,
        fontWeightName: '200'
    },
    {
        fontWeight: 300,
        fontWeightName: '300'
    },
    {
        fontWeight: 400,
        fontWeightName: '400, normal'
    },
    {
        fontWeight: 500,
        fontWeightName: '500'
    },
    {
        fontWeight: 600,
        fontWeightName: '600'
    },
    {
        fontWeight: 700,
        fontWeightName: '700, bold'
    },
    {
        fontWeight: 800,
        fontWeightName: '800'
    },
    {
        fontWeight: 900,
        fontWeightName: '900'
    }
];

export const mainPageData: SiteSettingsDataModel[] = [
    {
        sectionId: 1,
        sectionKey: 'cover',
        sectionName: 'Обложка',
        sectionHint: 'Текст подсказка для обложки',
        sectionDataFields: [
            {
                fieldId: 1,
                fieldType: FormFiledTypesEnum.textarea,
                fieldKey: 'coverHeader',
                fieldLabel: 'Заголовок',
                fieldValue: 'Заголовок',
                fieldHints: 'Текст подсказка для заголовка. Минимальная длина - 30 символов, максимальная - 200',
                validators: {
                    required: true,
                    minLength: 30,
                    maxLength: 200
                }
            },
            {
                fieldId: 2,
                fieldType: FormFiledTypesEnum.textarea,
                fieldKey: 'coverDesc',
                fieldLabel: 'Описание обложки',
                fieldValue: 'Описание',
                fieldHints: 'Текст подсказка для описания обложки',
                validators: {
                    required: true
                }
            },
        ]
    },
    {
        sectionId: 2,
        sectionKey: 'contacts',
        sectionName: 'Контакты',
        sectionHint: 'Текст подсказка для контактов',
        sectionDataFields: [
            {
                fieldId: 3,
                fieldType: FormFiledTypesEnum.textarea,
                fieldKey: 'contactAddress',
                fieldLabel: 'Адрес',
                fieldValue: '',
                fieldHints: 'Город, улица, номер дома',
                validators: {
                    required: true,
                    minLength: 30,
                    maxLength: 200
                }
            },
            {
                fieldId: 4,
                fieldType: FormFiledTypesEnum.textarea,
                fieldKey: 'contactWorkingHours',
                fieldLabel: 'Время работы',
                fieldValue: '',
                fieldHints: 'Укажите в одну строку',
                validators: {
                    required: true
                }
            },
            {
                fieldId: 5,
                fieldType: FormFiledTypesEnum.input,
                fieldKey: 'contactPhone',
                fieldLabel: 'Телефон',
                fieldValue: '',
                validators: {
                    required: true
                }
            },
            {
                fieldId: 6,
                fieldType: FormFiledTypesEnum.input,
                fieldKey: 'contactEmail',
                fieldLabel: 'Email',
                fieldValue: '',
                validators: {
                    required: true
                }
            },
        ]
    }
]

export const siteDomainData: SiteDomainSettingsModel = {
    domainName: ''
};

export const paymentSystemSettings: PaymentSystemSettingsModel = {
    publishableKey: '',
    secretKey: '',
    webhookUrl: 'https://forms.web2headcdn.com/payment/stripe/webhook/',
    signingSecret: ''
};

export const subscriptionData: UserSubscriptionDataModel = {
    isActive: true,
    validThrough: new Date(),
    nextPaymentDate: new Date()
};

export const canceledSubscriptionData: UserSubscriptionDataModel = {
    isActive: false,
    validThrough: new Date(),
    nextPaymentDate: null
};

export const tariffPlans: TariffPlanModel[] = [
    {
        name: 'На месяц',
        price: '2 500 ₽',
        btnText: 'Оплатить'
    },
    {
        name: 'На месяц',
        price: '20 000 ₽',
        btnText: 'Оплатить'
    },
    {
        name: 'Навсегда',
        price: '*** ₽',
        btnText: 'Заказать'
    }
]
