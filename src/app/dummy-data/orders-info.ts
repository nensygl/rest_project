import {OrderListModel, OrderModel} from '../models/orders.model';
import {OrderStatusEnum} from '../enums/order-status.enum';

let date = new Date;
const hourInMs = 3600000;

export const ordersList: OrderListModel[] = [
    {
        orderId: 1,
        orderNo: 1,
        creationDate: new Date(date.getTime() - hourInMs),
        orderStatus: 1
    },
    {
        orderId: 2,
        orderNo: 2,
        creationDate: new Date(date.getTime() - (hourInMs * 1.5)),
        orderStatus: 2
    },
    {
        orderId: 3,
        orderNo: 3,
        creationDate: new Date(date.getTime() - (hourInMs * 2)),
        orderStatus: 3
    },
    {
        orderId: 4,
        orderNo: 4,
        creationDate: new Date(date.getTime() - (hourInMs * 2.5)),
        orderStatus: 4
    },
    {
        orderId: 5,
        orderNo: 5,
        creationDate: new Date(date.getTime() - (hourInMs * 3)),
        orderStatus: 5
    },
    {
        orderId: 6,
        orderNo: 6,
        creationDate: new Date(date.getTime() - (hourInMs * 3.5)),
        orderStatus: 6
    },{
        orderId: 7,
        orderNo: 7,
        creationDate: new Date(date.getTime() - (hourInMs * 4)),
        orderStatus: 4
    },
    {
        orderId: 8,
        orderNo: 8,
        creationDate: new Date(date.getTime() - (hourInMs * 4.5)),
        orderStatus: 1
    },
    {
        orderId: 9,
        orderNo: 9,
        creationDate: new Date(date.getTime() - (hourInMs * 5)),
        orderStatus: 6
    },{
        orderId: 10,
        orderNo: 10,
        creationDate: new Date(date.getTime() - hourInMs * 5.5),
        orderStatus: 3
    }
];

export const selectedOrder: OrderModel = {
    orderId: 1,
    orderNo: 1,
    creationDate: new Date(date.getTime() - (hourInMs * 1.5)),
    orderStatus: 1,
    orderList: [
        {
            itemName: 'Креветки темпура',
            itemQuantity: 1,
            itemPrice: 360
        },
        {
            itemName: 'Страчателла с томатами Кимчи',
            itemQuantity: 2,
            itemPrice: 340
        },
        {
            itemName: 'Пад Тай',
            itemQuantity: 1,
            itemPrice: 480
        },
        {
            itemName: 'Удон с курицей',
            itemQuantity: 1,
            itemPrice: 330
        },
        {
            itemName: 'Вагаси Моти манго',
            itemQuantity: 5,
            itemPrice: 150
        }
    ],
    deliveryPrice: 200,
    totalPrice: 2800,
    contact: {
        contactName: 'Михаил',
        contactPhone: '+79053242155',
        orderComment: 'Позвоните за 20 минут, до того как приедете!',
        addressLine1: 'Кутузова, 17',
        addressLine2: 'Квартира 90',
        paymentMethod: 1,
        paymentStatus: 1
    },
    statusHistory: [
        {
            status: 1,
            statusChangeTime: new Date(date.getTime() - (hourInMs * 1.5))
        }
    ]
}

export const canceledOrder: OrderModel = {
    orderId: 1,
    orderNo: 1,
    creationDate: new Date(date.getTime() - (hourInMs * 1.5)),
    orderStatus: OrderStatusEnum.canceled,
    orderList: [
        {
            itemName: 'Креветки темпура',
            itemQuantity: 1,
            itemPrice: 360
        },
        {
            itemName: 'Страчателла с томатами Кимчи',
            itemQuantity: 2,
            itemPrice: 340
        },
        {
            itemName: 'Пад Тай',
            itemQuantity: 1,
            itemPrice: 480
        },
        {
            itemName: 'Удон с курицей',
            itemQuantity: 1,
            itemPrice: 330
        },
        {
            itemName: 'Вагаси Моти манго',
            itemQuantity: 5,
            itemPrice: 150
        }
    ],
    deliveryPrice: 200,
    totalPrice: 2800,
    contact: {
        contactName: 'Михаил',
        contactPhone: '+79053242155',
        orderComment: 'Позвоните за 20 минут, до того как приедете!',
        addressLine1: 'Кутузова, 17',
        addressLine2: 'Квартира 90',
        paymentMethod: 1,
        paymentStatus: 1
    },
    statusHistory: [
        {
            status: OrderStatusEnum.created,
            statusChangeTime: new Date(date.getTime() - (hourInMs * 1.5))
        },
        {
            status: OrderStatusEnum.canceled,
            statusChangeTime: new Date(date.getTime())
        }
    ]
}

export const selectedCookingOrder: OrderModel = {
    orderId: 1,
    orderNo: 1,
    creationDate: new Date(date.getTime() - (hourInMs * 1.5)),
    orderStatus: OrderStatusEnum.cooking,
    orderList: [
        {
            itemName: 'Креветки темпура',
            itemQuantity: 1,
            itemPrice: 360
        },
        {
            itemName: 'Страчателла с томатами Кимчи',
            itemQuantity: 2,
            itemPrice: 340
        },
        {
            itemName: 'Пад Тай',
            itemQuantity: 1,
            itemPrice: 480
        },
        {
            itemName: 'Удон с курицей',
            itemQuantity: 1,
            itemPrice: 330
        },
        {
            itemName: 'Вагаси Моти манго',
            itemQuantity: 5,
            itemPrice: 150
        }
    ],
    deliveryPrice: 200,
    totalPrice: 2800,
    contact: {
        contactName: 'Михаил',
        contactPhone: '+79053242155',
        orderComment: 'Позвоните за 20 минут, до того как приедете!',
        addressLine1: 'Кутузова, 17',
        addressLine2: 'Квартира 90',
        paymentMethod: 1,
        paymentStatus: 1
    },
    statusHistory: [
        {
            status: OrderStatusEnum.created,
            statusChangeTime: new Date(date.getTime() - (hourInMs * 1.5))
        },
        {
            status: OrderStatusEnum.cooking,
            statusChangeTime: new Date(date.getTime())
        }
    ]
}

export const selectedInDeliveryOrder: OrderModel = {
    orderId: 1,
    orderNo: 1,
    creationDate: new Date(date.getTime() - (hourInMs * 1.5)),
    orderStatus: OrderStatusEnum.inDelivery,
    orderList: [
        {
            itemName: 'Креветки темпура',
            itemQuantity: 1,
            itemPrice: 360
        },
        {
            itemName: 'Страчателла с томатами Кимчи',
            itemQuantity: 2,
            itemPrice: 340
        },
        {
            itemName: 'Пад Тай',
            itemQuantity: 1,
            itemPrice: 480
        },
        {
            itemName: 'Удон с курицей',
            itemQuantity: 1,
            itemPrice: 330
        },
        {
            itemName: 'Вагаси Моти манго',
            itemQuantity: 5,
            itemPrice: 150
        }
    ],
    deliveryPrice: 200,
    totalPrice: 2800,
    contact: {
        contactName: 'Михаил',
        contactPhone: '+79053242155',
        orderComment: 'Позвоните за 20 минут, до того как приедете!',
        addressLine1: 'Кутузова, 17',
        addressLine2: 'Квартира 90',
        paymentMethod: 1,
        paymentStatus: 1
    },
    statusHistory: [
        {
            status: OrderStatusEnum.created,
            statusChangeTime: new Date(date.getTime() - (hourInMs * 1.5))
        },
        {
            status: OrderStatusEnum.cooking,
            statusChangeTime: new Date(date.getTime() - hourInMs)
        },
        {
            status: OrderStatusEnum.givenToDelivery,
            statusChangeTime: new Date(date.getTime() - (hourInMs * 0.5))
        },
        {
            status: OrderStatusEnum.inDelivery,
            statusChangeTime: new Date(date.getTime() - (hourInMs * 0.2))
        }
    ]
}
