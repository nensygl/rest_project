import {MenuCategoryModel, MenuItemModel, MenuModel} from '../models/menu.model';

export const menuInfo: Array<MenuModel> = [
    {
        id: 4,
        categoryName: 'Закуски',
        categoryPosition: 4,
        menuItems: [
            {
                id: 7,
                categoryId: 4,
                itemPosition: 1,
                itemName: 'Креветки темпура',
                itemPrice: 360,
                description: 'Креветки в хрустящем кляре, соус сладко-соевый терияки с ароматом имбиря, грибы Эноки, фреш лайма, лук зеленый, кунжут острый Кимчи, приправа острая Тогараши',
                isHidden: false,
                itemImgs: []
            },
            {
                id: 8,
                categoryId: 4,
                itemPosition: 2,
                itemName: 'Бао с креветками и еще бог значем чем, что увеличит до трех строк',
                itemPrice: 330,
                description: 'Булочка пшеничная, креветки в хрустящем кляре, соус цитрусовый с икрой тобико, соус карамельно-соевый, помидоры с кинзой, редька дайкон, лук зеленый, острый кунжут кимчи',
                isHidden: false,
                itemImgs: []
            },
            {
                id: 9,
                categoryId: 4,
                itemPosition: 3,
                itemName: 'Димсамы с креветками',
                itemPrice: 370,
                description: 'Димсамы из крахмального теста с начинкой из креветок, крабовых палочек, японского майонеза, сливочного сыра, икры масаго, соус соево-карамельный, лук зеленый, кунжут кимчи.',
                isHidden: true,
                itemImgs: []
            },
            {
                id: 10,
                categoryId: 4,
                itemPosition: 4,
                itemName: 'Страчателла с томатами Кимчи',
                itemPrice: 340,
                description: 'Сыр страчателла, томаты, шпинат с соусом Севиче, лук зеленый, кунжут кимчи.',
                isHidden: false,
                itemImgs: []
            },
        ]
    },
    {
        id: 1,
        categoryName: 'Вок',
        categoryPosition: 1,
        menuItems: [
            {
                id: 1,
                categoryId: 1,
                itemPosition: 1,
                itemName: 'Пад Тай',
                itemPrice: 480,
                description: 'Рисовая лапша с куриным филе, креветками, яичным омлетом, зеленым луком, ростками сои, соусом Пад Тай, песто по-азиатски, жареный арахис с чили, микс жареных орехов кешью и арахис, кинза и лайм.',
                isHidden: true,
                itemImgs: []
            },
            {
                id: 2,
                categoryId: 1,
                itemPosition: 2,
                itemName: 'Удон с курицей',
                itemPrice: 330,
                description: 'Жареное на воке блюдо. Лапша пшеничная, филе куриной грудки, морковь, перец болгарский, фасоль стручковая, соус соево-устричный с добавлением кетчупа и сладкого чили, масло каффировое, лук зеленый, кунжут',
                isHidden: false,
                itemImgs: []
            },
            {
                id: 3,
                categoryId: 1,
                itemPosition: 3,
                itemName: 'Японский карри с курицей',
                itemPrice: 390,
                description: 'Рис в сладкой уксусной заправке, куриное филе в хрустящей панировке Панко, соус карри с добавлением кокосового молока, огурцы Кимчи, помидоры с кинзой, лайм, лук зеленый, кунжут, приправа Тогараши',
                isHidden: false,
                itemImgs: []
            }
        ]
    },
    {
        id: 2,
        categoryName: 'Поке',
        categoryPosition: 2,
        menuItems: [
            {
                id: 4,
                categoryId: 2,
                itemPosition: 1,
                itemName: 'Поке с креветками',
                itemPrice: 530,
                description: 'Рис в сладкой уксусной заправке, креветки в хрустящей панировке с острым соусом Кимчи, огурцы Кимчи, помидоры с кинзой, авокадо, шпинат, соус ореховый, заправка медово-цитрусовая, перец чили маринованный, лук зеленый, хлопья водорослей Нори, острый кунжут Кимчи',
                isHidden: true,
                itemImgs: []
            },
            {
                id: 5,
                categoryId: 2,
                itemPosition: 2,
                itemName: 'Поке с лососем',
                itemPrice: 530,
                description: 'Филе лосося, рис, шпинат, авокадо, краснокачанная капуста, дайкон, чука, соус цитрусовый с тобико, заправка медово-цитрусовая, икра летучей рыбы, хлопья нори, кунжут Кимчи',
                isHidden: false,
                itemImgs: []
            }
        ]
    },
    {
        id: 3,
        categoryName: 'Десерты',
        categoryPosition: 3,
        menuItems: [
            {
                id: 6,
                categoryId: 3,
                itemPosition: 1,
                itemName: 'Вагаси Моти манго',
                itemPrice: 150,
                description: 'Рисовая мука, крахмал, сливочный крем, манго, пюре из манго',
                isHidden: false,
                itemImgs: []
            }
        ]
    }
];

export let menuCategoriesList: Array<MenuCategoryModel> = [
    {
        id: 4,
        categoryName: 'Закуски',
        categoryPosition: 0
    },
    {
        id: 1,
        categoryName: 'Вок',
        categoryPosition: 1
    },
    {
        id: 2,
        categoryName: 'Поке',
        categoryPosition: 2
    },
    {
        id: 3,
        categoryName: 'Десерты',
        categoryPosition: 3
    },
    {
        id: 8,
        categoryName: 'Напитки',
        categoryPosition: 4
    },
    {
        id: 5,
        categoryName: 'Завтраки',
        categoryPosition: 5
    },
    {
        id: 6,
        categoryName: 'Боулы',
        categoryPosition: 6
    },
    {
        id: 7,
        categoryName: 'Выбор шефа',
        categoryPosition: 7
    }
];

export const menuItem: MenuItemModel = {
    id: 5,
    categoryId: 2,
    itemPosition: 2,
    itemName: 'Поке с лососем',
    itemPrice: 530,
    description: 'Филе лосося, рис, шпинат, авокадо, краснокачанная капуста, дайкон, чука, соус цитрусовый с тобико, заправка медово-цитрусовая, икра летучей рыбы, хлопья нори, кунжут Кимчи',
    isHidden: false,
    itemImgs: []
};

export const menuItemLong: MenuItemModel = {
    id: 8,
    categoryId: 4,
    itemPosition: 2,
    itemName: 'Бао с креветками и еще бог значем чем, что увеличит до трех строк',
    itemPrice: 330,
    description: 'Булочка пшеничная, креветки в хрустящем кляре, соус цитрусовый с икрой тобико, соус карамельно-соевый, помидоры с кинзой, редька дайкон, лук зеленый, острый кунжут кимчи',
    isHidden: false,
    itemImgs: []
};
