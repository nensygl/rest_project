export class SelectionInfoModel {
    optionId: number;
    isSelected: boolean;
}

export class SelectedSectionModel {
    sectionId: number;
    selectionInfo: SelectionInfoModel[];
}

export class DisplaySectionsInfoModel {
    sectionId: number;
    displayingItemNo: number;
    sectionItemsLen: number;
}
