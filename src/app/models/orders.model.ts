import {TuiDay} from '@taiga-ui/cdk';

export class OrderListModel {
    orderId: number;
    orderNo: number;
    creationDate: Date;
    orderStatus: number;
}

export class OrderMenuItemModel {
    itemName: string;
    itemQuantity: number;
    itemPrice: number;
}

export class ContactInfoModel {
    contactName: string;
    contactPhone: string;
    orderComment: string;
    addressLine1: string;
    addressLine2: string;
    paymentMethod: number;
    paymentStatus: number;
}

export class StatusHistoryModel {
    status: number;
    statusChangeTime: Date;
}

export class OrderModel {
    orderId: number;
    orderNo: number;
    creationDate: Date;
    orderStatus: number;
    orderList: OrderMenuItemModel[];
    deliveryPrice: number;
    totalPrice: number;
    contact: ContactInfoModel;
    statusHistory: StatusHistoryModel[];
}

export class FiltersModel {
    label: string;
    value: number;
    checked: boolean;
}

export class OrdersListFiltersModel {
    date: TuiDay;
    filters: FiltersModel[];
}
