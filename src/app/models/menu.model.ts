export class MenuItemModel {
    id: number;
    categoryId: number;
    itemPosition: number;
    itemName: string;
    itemPrice: number;
    description: string;
    isHidden: boolean;
    itemImgs: Array<any>;
    quantity?: number;
}

export class MenuModel {
    id: number;
    categoryName: string;
    categoryPosition: number;
    menuItems: Array<MenuItemModel>
}

export class MenuCategoryModel {
    id: number;
    categoryName: string;
    categoryPosition: number;
}
