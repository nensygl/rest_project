export class SiteGeneralSettingsModel {
    favicon: any;
    siteName: string;
    siteDescription: string;
    placeFormat: string;
}

export class FontListItemModel {
    fontId: number;
    fontName: string;
}

export class FontWeightItemModel {
    fontWeight: number;
    fontWeightName: string;
}

export class TypographyValuesModel {
    fontFamily: number;
    fontColor: string;
    fontWeight: number;
    fontSize: number;
}

export class SiteColorsAndFontsSettingsModel {
    header: TypographyValuesModel;
    text: TypographyValuesModel;
    buttons: TypographyValuesModel;
    backgroundColor: string;
}

export class MenuCardsFormatSettingsModel {
    activeCardFormatId: number;
}

export interface JsonFormValidators {
    min?: number;
    max?: number;
    required?: boolean;
    requiredTrue?: boolean;
    email?: boolean;
    minLength?: number;
    maxLength?: number;
    pattern?: string;
    nullValidator?: boolean;
}

export class FieldDataModel {
    fieldId: number;
    fieldType: number;
    fieldKey: string;
    fieldLabel: string;
    fieldValue: string | number;
    fieldHints?: string;
    validators: JsonFormValidators;
}

export class SiteSettingsDataModel {
    sectionId: number;
    sectionKey: string;
    sectionName: string;
    sectionHint: string;
    sectionDataFields: FieldDataModel[];
}

export class SiteHeaderSettingsModel {

}

export class SiteDomainSettingsModel {
    domainName: string;
}

export class PaymentSystemSettingsModel {
    publishableKey: string;
    secretKey: string;
    webhookUrl: string;
    signingSecret: string;
}

export class UserSubscriptionDataModel {
    isActive: boolean;
    validThrough: Date | null;
    nextPaymentDate: Date | null;
}

export class TariffPlanModel {
    name: string;
    price: string;
    btnText: string;
}
